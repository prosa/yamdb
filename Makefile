DATADIR := yamdb/data
DATA := $(DATADIR)/*.yml
DATA += $(DATADIR)/*.bib
PROCDIR := yamdb/properties
SRC := $(PROCDIR)/*.py
SRC += yamdb/yamdb.py
SRC += yamdb/yamref.py
SRC += tests/unit-tests/density/*.py
SRC += tests/integration-tests/*.py
SRC += MANIFEST.in
DOCSRC += docs/source/*.rst
DOCSRC += docs/source/*.py
DOCSRC += docs/source/Makefile
DOCSRC += docs/Makefile
TARGET := dist/yamdb-0.3.0.tar.gz 
PKGBUILD := contrib/arch/PKGBUILD

$(PKGBUILD): $(TARGET)
	TEMP=`mktemp`; \
	cp $(PKGBUILD) $$TEMP; \
	SHA256SUM=`sha256sum $(TARGET) | cut -d" " -f1`; \
	sed -e "s/^sha256sums.*/sha256sums=('$$SHA256SUM')/g" < $$TEMP > $(PKGBUILD)

$(TARGET): $(DATA) $(SRC) DOC
	rm -rf .mypy_cache; \
	find yamdb -name "*~" -exec rm  -i {} \; ; \
	find yamdb -name "auto" -exec rm -ri {} \; ; \
	find yamdb -name __pycache__ -exec rm -ri {} \; ; \
	python setup.py sdist

DOC: $(DOCSRC)
	cd docs/source/; \
	make

clean:
	rm -f $(TARGET) ; \
	find yamdb -name "*~" -exec rm  -i {} \; ; \
	find yamdb -name "auto" -exec rm -ri {} \; ; \
	find yamdb -name __pycache__ -exec rm -ri {} \; ; \
	rm -rf contrib/arch/pkg ; \
	rm -rf contrib/arch/src ; \
	rm -rf contrib/arch/python-yamdb-*-any.pkg.tar*
