.. _installation:

Installation
============

After untaring the package, move to its root directory (it should
contain setup.py and setup.cfg) and enter

.. code-block:: shell

  python -m pip install .

or (if pip is missing):

.. code-block:: shell

  python setup.py install		

If you are using `Arch Linux <https://archlinux.org/>`_ or derived
distributions, you can compile the supplied `PKGBUILD
<https://wiki.archlinux.org/title/PKGBUILD>`_ into an Arch package
(extension may vary according to your configuration) and install the
latter with

.. code-block:: shell

  cd contrib/arch
  makepkg -p PKGBUILD.use_repository
  sudo pacman -U python-yamdb-*-any.pkg.tar*

or - avoiding an additional download - with

.. code-block:: shell

  make
  cd contrib/arch
  makepkg
  sudo pacman -U python-yamdb-*-any.pkg.tar*

The integration tests run by the check() function require the presence
of `bibtexparser
<https://github.com/sciunto-org/python-bibtexparser/>`_. If it is not
present on your system, either install bibtexparser or remove the
check function. You can still run the unit tests separately. Testing
requires `pytest <https://docs.pytest.org/en/latest/>`_.
