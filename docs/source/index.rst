.. Yamdb documentation master file, created by
   sphinx-quickstart on Fri Aug 18 21:05:34 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Yamdb
=====

Yamdb is a MIT-licensed `Python <https:www.python.org>`_ package to easily access thermophysical properties of liquid metals and molten salts.


Yamdb Documentation Contents
============================

.. toctree::
   :maxdepth: 2

   installation	    
   usage
   modules
   history
	     
Indices and tables
^^^^^^^^^^^^^^^^^^

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
