import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992I2

# KF-ZrF4 from Janz 1988
# x_KF = 0.667 - 1.0 (range)
# x_KF = 0.7 -> x_ZrF4 = 0.3 -> 30 mol%
# Janz et al (1988) p. 211: 2.9934 ohm^{-1} cm^{-1}
coef = {
    'D1': 5.85,
    'D2': -0.04785,
    'D3': -0.001579,
    'component': 'second',
}

rho_e = Linstrom1992I2(1233.2, 70, coef=coef)

def test():
    assert 0.01/rho_e == approx(2.9934, 0.0000001)
