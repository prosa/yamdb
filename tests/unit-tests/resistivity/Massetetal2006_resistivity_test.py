import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Massetetal2006

# LiCl-LiF-LiI from Massetetal2006
# 29.1-11.7-59.2
# Masset et al (2006) p. 755, Tab. 2:  2.433172048 ohm^{-1} cm^{-1}
coef = {
    'A': 8.895,
    'E': -872.6,
}

rho_e = Massetetal2006(400 + 273.15, coef=coef)

def test():
    assert 0.01/rho_e == approx(2.433172048, 1.0E-09)
