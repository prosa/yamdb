import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992P1

# KCl-LiCl-NaCl from Janzetal1979
# 40.4-55.8-3.8
# Janz et al (1979) p. 334:  1.020 ohm^{-1} cm^{-1}
coef = {
    'D1': -3.80334,
    'D2': 7.59545E-03,
}

rho_e = Linstrom1992P1(635, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.020, 0.0003)
