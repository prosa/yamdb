import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import SalyulevPotapov2015

# Salyulev, Potapov (2015) ZnCl2
# J. Chemical and Engineering Data (2015) 60, 484-492, Eq. (2)
coef = {'a': 0.26654,
        'b': 381.67,
        'c': -3.4064E+05,
        'd': -5.7406E+08,
        }

rho_e = SalyulevPotapov2015(1200.0, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.037127451, 0.0000001)
