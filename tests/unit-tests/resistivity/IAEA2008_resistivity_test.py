import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import IAEA2008

# Hg from IAEA (2008)
coef = {
    'a': 1,
    'b': 0.8896E-03,
    'c': 1.0075E-06,
    'd': -1.05E-10,
    'e': 2.702E-13,
    'f': 1.199E-15,
    'rho_e0': 0.9407E-06,
}

rho_e = IAEA2008(300, coef=coef)

def test():
    assert rho_e == approx(9.6385e-07)
