import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Sobolev2011

# Iida, Guthrie (2015) Rb
# Vol II, p. 540, Tab. 17.11/GT, fit to data
coef = {'a': 3.06821E-08,
        'b': 4.77161E-10,
        'c': 4.84378E-13,
        }

rho_e = Sobolev2011(700.0, coef=coef)

def test():
    assert rho_e == approx(6.0204E-07, 0.0000001)
