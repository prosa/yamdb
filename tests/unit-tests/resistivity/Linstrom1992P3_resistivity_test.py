import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992P3

# AlBr3-NaBr from Janz 1988
# 92-8
# Janz et al (1988) p. 165: 0.3341 ohm^{-1} cm^{-1}
coef = {
    'D1': -2.525,
    'D2': 0.0060275,
    'D3': -3.9683e-06,
    'D4': 7.999e-10,
}

rho_e = Linstrom1992P3(1000.0, coef=coef)

def test():
    assert 0.01/rho_e == approx(0.3341, 0.0000001)
