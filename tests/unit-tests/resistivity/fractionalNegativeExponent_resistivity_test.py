import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import fractionalNegativeExponent

# Te from
# Iida, Guthrie (2015)
coef = {
    'a': 3.76373E-06,
    'b': 6.55205E+12,
    'c': 6.50001,
}

rho_e = fractionalNegativeExponent(1100, coef=coef)

def test():
    assert rho_e == approx(3.8752E-06, 1.0E-05)
