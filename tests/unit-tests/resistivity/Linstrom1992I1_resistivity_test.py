import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992I1

# BeF2-Na3AlF6 from Janz 1988
# x_BeF2 = 0.0 - 0.45 (range)
# x_BeF2 = 0.4 -> x_Na3AlF6 = 0.6 -> 60 mol%
# Janz et al (1988) p. 175: 2.2612 ohm^{-1} cm^{-1}
coef = {
    'D1': 1.495,
    'D2': 0.01277,
    'component': 'second',
}

rho_e = Linstrom1992I1(1273.0, 40, coef=coef)

def test():
    assert 0.01/rho_e == approx(2.2612, 0.0000001)
