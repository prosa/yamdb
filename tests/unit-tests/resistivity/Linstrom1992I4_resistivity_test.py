import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992I4

# TiF4-XeF2 from Janz 1988
# x_XeF2 = 0.014-0.07 (range)
# x_TiF4 = 0.95 -> x_XeF2 = 0.05 -> 5 mol%
# Janz et al (1988) p. 250: 5.42125E-04 ohm^{-1} cm^{-1}
coef = {
    'D1': -0.0008485,
    'D2': 0.001286,
    'D3': -0.000651,
    'D4': 0.0001342,
    'D5': -8.863e-06,
    'component': 'second',
}

rho_e = Linstrom1992I4(405.0, 95, coef=coef)

def test():
    assert 0.01/rho_e == approx(5.42125E-04, 0.0000001)
