import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992I3

# RbCl-TiCl3 from Janz 1988
# x_RbCl = 0.5 - 1.0 (range)
# x_RbCl = 0.7 -> x_TiCl3 = 0.3 -> 30 mol%
# Janz et al (1988) p. 247: 1.0029045 ohm^{-1} cm^{-1}
coef = {
    'D1': 1.6794,
    'D2': -0.034436,
    'D3': 0.00041854,
    'D4': -7.445e-07,
    'component': 'second',
}

rho_e = Linstrom1992I3(1073.0, 70, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.0029045, 0.0000001)
