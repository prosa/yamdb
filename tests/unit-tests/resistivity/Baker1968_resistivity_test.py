import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Baker1968

# Se from Baker (1968)
coef = {
    'A': 6000,
    'B': 3.80,
}

rho_e = Baker1968(800, coef=coef)

def test():
    assert rho_e == approx(50.119, 0.00001)
