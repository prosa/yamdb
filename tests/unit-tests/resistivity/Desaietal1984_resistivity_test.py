import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Desaietal1984

# Zn from Desai et al. (1984)
coef = {
    'a': 40.81824,
    'b': 8.51402E-03,
    'c': -3.36074E-05,
    'd': 2.06737E-08,
}

rho_e = Desaietal1984(1300, coef=coef)

def test():
    assert rho_e == approx(4.051E-07)
