import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Janzetal1968

# KI from Janz et al. 1968
# Janz et al (1968) p. 76, Tab. 86
coef = {
    'a': -6.1952,
    'b': 12.6232E-03,
    'c': -5.0591E-06,
}

rho_e = Janzetal1968(1100, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.569, 0.0004)
