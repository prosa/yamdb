import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Janz1967exp

# KCl-LiCl-NaCl from Janzetal1979
# 42.0-57.9-0.1
# Janz et al (1979) p. 334:  1.064 ohm^{-1} cm^{-1}
coef = {
    'A': 43.70816,
    'E': 4687.94E-03,
}

rho_e = Janz1967exp(635, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.064, 0.0004)
