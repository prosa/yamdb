import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992plusE

# LiI from Janz 1992
# tested against molten.exe (DOS)
coef = {
    'D1': 10.113,
    'D2': -5903.72,
}

rho_e = Linstrom1992plusE(800, coef=coef)

def test():
    assert 0.01/rho_e == approx(4.163052, 0.0004)
