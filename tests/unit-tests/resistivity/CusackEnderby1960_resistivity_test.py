import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import CusackEnderby1960

# Ag from Iida, Guthrie (1988)
coef = {
    'alpha': 0.0090E-08,
    'beta': 6.2E-08,
}

rho_e = CusackEnderby1960(1300, coef=coef)

def test():
    assert rho_e == approx(1.79E-07)
