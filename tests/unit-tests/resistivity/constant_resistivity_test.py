import sys
sys.path.insert(0, '../../../')

from yamdb.properties.resistivity import constant

# Se from Baker (1968)
coef = {
    'value': 1.0E-06,
}

rho_e = constant(800, coef=coef)

def test():
    assert rho_e == 1.0E-06
