import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992P2

# AlBr3-NaBr from Janz 1988
# 50-50
# Janz et al (1988) p. 165: 1.0097 ohm^{-1} cm^{-1}
coef = {
    'D1': -1.0423,
    'D2': 0.0037731,
    'D3': -1.7211E-06,
}

rho_e = Linstrom1992P2(1000.0, coef=coef)

def test():
    assert 0.01/rho_e == approx(1.0097, 0.0000001)
