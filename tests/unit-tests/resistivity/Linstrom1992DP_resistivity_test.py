import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Linstrom1992DP

# Na3AlF6 from Janz 1992
# Al2O3-Na3AlF6-SiO2 0-100-0
coef = {
    'D1': 2.8,
    'Tmin': 1273.0,
    'Tmax': 1273.0,
}

rho_e = Linstrom1992DP(1273, coef=coef)

def test():
    assert 0.01/rho_e == approx(2.8)
