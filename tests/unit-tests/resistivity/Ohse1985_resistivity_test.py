import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Ohse1985

# Rb Ohse (1985)
coef = {'range': [{'a': 1.353,
                   'b': 1.051,
                   'c': 0.485,
                   'd': -0.498,
                   'Tmin': 312.6,
                   'Tmax': 611.7,
                   },
                  {'a': 1.689,
                   'b': 1.207,
                   'c': 0.049,
                   'd': 4.138,
                   'Tmin': 611.7,
                   'Tmax': 1087.7,
                   },
                  {'a': 2.057,
                   'b': 0.880,
                   'c': 3.153,
                   'd': -0.531,
                   'Tmin': 1087.7,
                   'Tmax': 1500,
                   }]
        }

rho_e = Ohse1985(1200.0, coef=coef)

def test():
    assert rho_e == approx(1.2597E-06, 0.00005)
