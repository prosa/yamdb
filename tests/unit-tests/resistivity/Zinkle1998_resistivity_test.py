import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.resistivity import Zinkle1998

# Iida, Guthrie (2015) Li
# Vol II, p. 540, Tab. 17.11/GT, fit to data
coef = {'a': -64.9,
        'b': 1.064,
        'c': -1.035E-03,
        'd': 5.33E-07,
        'e': -9.23E-12,
        }

rho_e = Zinkle1998(700.0, coef=coef)

def test():
    assert rho_e == approx(3.5335E-07, 0.00001)
