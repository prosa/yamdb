import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.vapour_pressure import Sobolev2011

# Pb from OECDNEA (2015)
coef = {
    'a': 5.76E+09,
    'b': -22131,
}

pv = Sobolev2011(1000.0, coef=coef)

def test():
    assert pv == approx(1.4095, 0.0001)
