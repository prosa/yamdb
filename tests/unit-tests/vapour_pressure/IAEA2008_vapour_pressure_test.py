import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.vapour_pressure import IAEA2008

# Hg from IAEA2008
coef = {
    'a': 33.197,
    'b': -7765.6,
    'c': -1.5337,
    'd': 0.864E-03,
}

pv = IAEA2008(400.0, coef=coef)

def test():
    assert pv == approx(139.7, 0.0001)
