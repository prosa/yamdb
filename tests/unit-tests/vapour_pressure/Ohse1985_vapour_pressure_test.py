import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.vapour_pressure import Ohse1985

# Mg from Iida, Guthrie (2015)
coef = {
    'A': 13.0719,
    'B': -18880.659,
    'C': -0.4942,
}

pv = Ohse1985(1100.0, coef=coef)

def test():
    assert pv == approx(524.4, 0.0001)
