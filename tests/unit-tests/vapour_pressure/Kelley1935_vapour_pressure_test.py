import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.vapour_pressure import Kelley1935

# CsCl from Kelley (1935) p. 39
coef = {
    'A': 46700.0,
    'B': 16.1,
    'C': 81.15,
}

pv = Kelley1935(1000.0, coef=coef)

def test():
    assert pv == approx(95.148, 0.0001)
