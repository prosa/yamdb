import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.vapour_pressure import IidaGuthrie2015

# Mg from Iida, Guthrie (2015)
coef = {
    'A': 12.79,
    'B': -7550,
    'C': -1.41,
    'unit': 'mmHg',
}

pv = IidaGuthrie2015(1000.0, coef=coef)

def test():
    assert pv == approx(1364.3, 0.0001)
