import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.sound_velocity import Blairs2007cubic

# Bi from  Blairs (2007)
coef = {
    'A': 1720,
    'B': -9.199E-02,
    'C': -4.628E-05,
}

v = Blairs2007cubic(600.0, coef=coef)

def test():
    assert v == approx(1648.1, 0.0001)
