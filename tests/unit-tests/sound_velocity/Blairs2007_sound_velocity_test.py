import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.sound_velocity import Blairs2007

# Bi from  Blairs (2007)
coef = {
    'A': 1758,
    'B': -0.164,
}

v = Blairs2007(600.0, coef=coef)

def test():
    assert v == approx(1659.6, 0.0001)
