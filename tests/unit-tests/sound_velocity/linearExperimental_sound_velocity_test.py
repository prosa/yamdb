import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.sound_velocity import linearExperimental

# Bi from Iida, Guthrie (1988)
coef = {
    'Um': 1620,
    'm_dUdT': 0.21,
    'Tm': 544.10,
}

v = linearExperimental(600.0, coef=coef)

def test():
    assert v == approx(1608.3, 0.0001)
