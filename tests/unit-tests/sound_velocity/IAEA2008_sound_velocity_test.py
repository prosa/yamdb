import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.sound_velocity import IAEA2008

# Hg from IAEA2008
coef = {
    'a': 1460,
    'b': -7765.6,
    'c': -0.4671,
}

v = IAEA2008(400.0, coef=coef)

def test():
    assert v == approx(1381.3, 0.0001)
