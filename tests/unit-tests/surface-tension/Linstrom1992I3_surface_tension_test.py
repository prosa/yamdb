import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992I3

# BaCl2-RbCl Janz 1988 p. 114 Tab. 2.2.a
# x_BaCl2 0.3 -> 30 mol%
coef = {
     'D1': 84.6224,
        'D2': 0.8923,
        'D3': -0.017576,
        'D4': 0.00018415,
        'Tmax': 1123.0,
        'Tmin': 1123.0,
        'component': 'first',
}

sigma = Linstrom1992I3(1123.0, 30, coef=coef)

def test():
    assert sigma*1000.0 == approx(100.54505, 0.0000001)
