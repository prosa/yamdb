import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992I4

# CaCl2-RbCl Janz 1988 p. 118 Tab. 2.2.a
# x_CaCl2 0.3 -> 30 mol%
coef = {
        'D1': 88.69,
        'D2': -0.1617,
        'D3': 0.015919,
        'D4': -0.00020322,
        'D5': 1.1869e-06,
        'Tmax': 1073.0,
        'Tmin': 1073.0,
        'component': 'first',
}

sigma = Linstrom1992I4(1073.0, 30, coef=coef)

def test():
    assert sigma*1000.0 == approx(93.640549, 0.0000001)
