import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992P2

# no case found for this function -> invented one
coef = {
    'D1': 100.0,
    'D2': -0.025,
    'D3': 0.000025,
}

sigma = Linstrom1992P2(800.0, coef=coef)

def test():
    assert sigma*1000.0 == approx(96, 0.0000001)
