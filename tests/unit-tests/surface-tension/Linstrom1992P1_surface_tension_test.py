import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992P1

# AgBr Janz 1988 p. 109 Tab. 2.2.a
coef = {
    'D1': 171.3,
    'D2': -0.025,
}

sigma = Linstrom1992P1(800.0, coef=coef)

def test():
    assert sigma*1000.0 == approx(151.3, 0.0000001)
