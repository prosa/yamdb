import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Sobolev2011

# Ga from Tanaka et al. (2004) p. 818 abstract
coef = {
    'a': 737,
    'b': -0.062,
}

sigma = Sobolev2011(900.0, coef=coef)

def test():
    assert sigma*1000.0 == approx(681.2, 0.0000001)
