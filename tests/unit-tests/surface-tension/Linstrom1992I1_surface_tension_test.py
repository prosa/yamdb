import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992I1

# NaCl-Na2SO4 Janz 1988 p. 142 Tab. 2.2.a
# x_NaCl 0.05 -> x_Na2SO4 0.95 -> 95 mol%
coef = {
    'D1': 69.65,
    'D2': 1.205,
    'Tmax': 1173.0,
    'Tmin': 1173.0,
    'component': 'second'
}

sigma = Linstrom1992I1(1173.0, 5, coef=coef)

def test():
    assert sigma*1000.0 == approx(184.125, 0.0000001)
