import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992I2

# BaBr2-NaBr Janz 1988 p. 112 Tab. 2.2.a
# x_NaCl 0.3 -> 30 mol%
coef = {
    'D1': 96.979,
    'D2': 0.4911,
    'D3': 0.00065272,
    'Tmax': 1123.0,
    'Tmin': 1123.0,
    'component': 'first'
}

sigma = Linstrom1992I2(1123.0, 30, coef=coef)

def test():
    assert sigma*1000.0 == approx(112.299448, 0.0000001)
