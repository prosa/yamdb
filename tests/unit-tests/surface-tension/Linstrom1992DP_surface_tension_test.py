import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import Linstrom1992DP

# MnCl2 seemingly not in Janz 1988 -> data from Janz 1992 used directly
coef = {
    'D1': 98.2,
    'Tmax': 1073.0,
    'Tmin': 1073.0,
}

sigma = Linstrom1992DP(1073.0, coef=coef)

def test():
    assert sigma*1000.0 == approx(98.2, 0.0000001)
