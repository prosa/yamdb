import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.surface_tension import IAEA2008

# Cs from Iida, Guthrie (2015)
coef = {
    'a': 69.0,
    'b': -0.047,
    'c': -301.55,
}

sigma = IAEA2008(400.0, coef=coef)

def test():
    assert sigma == approx(0.064373, 0.00001)
