import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.expansion_coefficient import Steinberg1974

# Au from Iida, Guthrie (1988)
coef = {
    'rho_m': 17400,
    'lambda': 1.45,
    'Tm': 1336.15,
}

beta = Steinberg1974(1400.0, coef=coef)

def test():
    assert beta == approx(8.3779E-05, 0.00001)
