import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.expansion_coefficient import IAEA2008

# Hg from IAEA2008
coef = {
    'a': 1.8144,
    'b': 7.016E-05,
    'c': 2.8625E-07,
    'd': 2.617E-10,
}

beta = IAEA2008(400.0, coef=coef)

def test():
    assert beta == approx(0.00018284, 0.0001)
