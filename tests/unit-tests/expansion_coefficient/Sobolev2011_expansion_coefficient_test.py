import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.expansion_coefficient import Sobolev2011

# SnCl2-ZnCl2 100-0 from Janz et al (1975)
coef = {
    'rho_s': 4072.0,
    'drhodT': -1.3,
}

beta = Sobolev2011(570.0, coef=coef)

def test():
    assert beta == approx(3.902731912E-04, 0.00001)
