import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.expansion_coefficient import OECDNEA2015

# Pb from OECDNEA (2015)
coef = {
    'a': 8942,
}

beta = OECDNEA2015(700.0, coef=coef)

def test():
    assert beta == approx(0.00012133, 0.00001)
