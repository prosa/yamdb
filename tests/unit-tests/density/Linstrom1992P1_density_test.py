import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992P1

# BaBr2-KBr 100-0 -> BaBr from Janz 1988 p. 18
# 1.020 g/cm^3 (Temperature range 1120-1120 K is already in Janz 1988)
coef = {
    'D1': 5.035,
    'D2': -0.000924,
}

rho = Linstrom1992P1(1120, coef=coef)


def test():
    assert rho/1000.0 == approx(4.00012, 0.0000001)
