import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992I2

# CsBr-LiBr from Janz 1988
# x_CsBr = 0.0 - 1.0 (range)
# x_CsBr = 0.3 -> x_LiBr = 0.7 -> 70 mol%
# Janz et al (1988) p. 33: 2.6104597 g/cm^3
coef = {
    'D1': 2.9385,
    'D2': -0.0027181,
    'D3': -2.8117e-05,
    'component': 'second',
}

rho = Linstrom1992I2(1073.0, 30, coef=coef)


def test():
    assert rho/1000.0 == approx(2.6104597, 0.0000001)
