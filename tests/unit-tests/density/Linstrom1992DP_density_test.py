import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992DP

# B2O3 from Janz 1992
# B2O3-NaF 100-0
coef = {
    'D1': 1.548,
    'Tmin': 1073.0,
    'Tmax': 1073.0,
}

rho = Linstrom1992DP(1073, coef=coef)


def test():
    assert rho/1000.0 == approx(1.548)
