import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992P2

# ZnCl2 Janz (1975)
# p. 899 Tab. 69
coef = {
    'D1': 3.0183,
    'D2': -1.0536E-03,
    'D3': 3.7641E-07,
}

rho = Linstrom1992P2(720, coef=coef)


def test():
    assert rho/1000.0 == approx(2.455, 0.0001)
