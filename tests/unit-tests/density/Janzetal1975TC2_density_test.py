import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Janzetal1975TC2

# SnCl2-ZnCl2 from Janzetal1975
coef = {
    'a': 2.79673,
    'b': -4.71427E-04,
    'c': 1.12048E-02,
    'd': -2.55020E-08,
    'e': -5.16503E-09,
}

rho = Janzetal1975TC2(560, 50, coef=coef)

def test():
    assert rho == approx(2977, 0.0003)
