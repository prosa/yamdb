import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Janzetal1975TC

# CaCl2-NaCl from Janzetal1975
coef = {
    'a': 2.06606,
    'b': -4.72915E-04,
    'c': 6.20063E-03,
    'd': -1.04771E-05,
}

rho = Janzetal1975TC(1070, 50, coef=coef)

def test():
    assert rho == approx(1844, 0.0001)
