import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992I4

# KBr-LiCl from Janz 1988
# x_KBr = 0.0 - 1.0 (range)
# x_KBr = 0.3 -> 30 mol%
# Janz et al (1988) p. 41: 1.72118762 g/cm^3
coef = {
    'D1': 1.403,
    'D2': 0.01596,
    'D3': -0.0002348,
    'D4': 2.106e-06,
    'D5': -7.598e-09,
    'component': 'first',
}

rho = Linstrom1992I4(1073.0, 30, coef=coef)


def test():
    assert rho/1000.0 == approx(1.72118762, 0.0000001)
