import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Shpilrain1985

# Na from Ohse (1985) rec
coef = {
    'a': [0.89660679,
          0.51613430,
          -0.18297218E+01,
          0.22016247E+01,
          -0.13975634E+01,
          0.44866894,
          -0.57963628E-01,
          ]
}

rho = Shpilrain1985(400.0, coef=coef)

def test():
    assert rho == approx(919.79, 0.0001)
