import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import IAEA2008

# Hg from IAEA2008
coef = {
    'rho_0': 13595.0,
    'a': 1.0,
    'b': -1.8144E-04,
    'c': -7.016E-09,
    'd': -2.8625E-11,
    'e': -2.617E-14,
}

rho = IAEA2008(400.0, coef=coef)

def test():
    assert rho == approx(13280, 0.0001)
