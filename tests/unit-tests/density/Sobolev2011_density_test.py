import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Sobolev2011

# Pb from Sobolev (2011)
coef = {
      'rho_s': 11441,
    'drhodT': -1.2795,
}

rho = Sobolev2011(700.0, coef=coef)


def test():
    assert rho == approx(10545, 0.0001)
