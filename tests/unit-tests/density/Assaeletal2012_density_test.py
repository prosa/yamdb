import sys
sys.path.insert(0, '../../../')

from yamdb.properties.density import Assaeletal2012

# Al from metals.yml
coef = {
    'c1': 2375.0,
    'c2': 0.233, 
    'Tref': 933.47,
    }

rho = Assaeletal2012(1000.0, coef=coef)

def test():
    assert rho == 2359.49851
