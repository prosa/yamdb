import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992I3

# CsCl-KI from Janz 1988
# x_CsCl = 0.0 - 1.0 (range)
# x_CsCl = 0.3 -> 30 mol% 
# Janz et al (1988) p. 33: 2.3794566 g/cm^3
coef = {
    'D1': 2.319,
    'D2': 0.002196,
    'D3': -1.196e-05,
    'D4': 1.978e-07,
    'component': 'first',
}

rho = Linstrom1992I3(1073.0, 30, coef=coef)


def test():
    assert rho/1000.0 == approx(2.3794566, 0.0000001)
