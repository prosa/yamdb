import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Linstrom1992I1

# CsBr-KCl from Janz 1988
# x_CsBr = 0.0 - 1.0 (range)
# x_CsBr = 0.3 -> 30 mol%
# Janz et al (1988) p. 33: 1.8027 g/cm^3
coef = {
    'D1': 1.449,
    'D2': 0.01179,
    'component': 'first',
}

rho = Linstrom1992I1(1073.0, 30, coef=coef)


def test():
    assert rho/1000.0 == approx(1.8027, 0.0000001)
