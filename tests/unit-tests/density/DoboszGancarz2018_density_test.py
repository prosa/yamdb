import sys
sys.path.insert(0, '../../../')

from yamdb.properties.density import DoboszGancarz2018

# Bi-Sn from DoboszGancarz2018 Tab. 2 
coef = {
    'a1': -0.826,
    'a2': 8952.1,
    }

rho = DoboszGancarz2018(1000.0, coef=coef)

def test():
    assert rho == 8126.1
