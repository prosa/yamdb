import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.density import Steinberg1974

# Sb from Iida, Guthrie (2015)
coef = {
    'rho_m': 6483,
    'lambda': 0.82,
    'Tm': 903.65,
}

rho = Steinberg1974(1000.0, coef=coef)


def test():
    assert rho == approx(6404, 0.00001)
