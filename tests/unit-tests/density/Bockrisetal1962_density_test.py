import sys
sys.path.insert(0, '../../../')

from yamdb.properties.density import Bockrisetal1962

# NaCl from salts_solid.yml
coef = {
    'a': 2168,
    'b': 0.1267, 
    'c': 1.754E-04,
    }

rho = Bockrisetal1962(1000.0, coef=coef)

def test():
    assert rho == 1983.2423691935
