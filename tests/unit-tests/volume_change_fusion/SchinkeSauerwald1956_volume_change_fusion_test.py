import sys
sys.path.insert(0, '../../../')

from yamdb.properties.volume_change_fusion import SchinkeSauerwald1956

# NaCl SchinkeSauerwald1956
coef = {
    'dV': 25.0,
    }

dV = SchinkeSauerwald1956(coef=coef)

def test():
    assert dV == 25.0
