import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import IAEA2008

# Hg from metals.yml
coef = {
    'a': 8.178,
    'b': 1.36E-02,
    'c': -6.378E-06,
    }

lambda_ = IAEA2008(400.0, coef=coef)

def test():
    assert lambda_ == approx(9.8005, 0.00005)
