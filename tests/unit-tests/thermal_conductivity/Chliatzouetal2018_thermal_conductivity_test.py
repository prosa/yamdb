import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import Chliatzouetal2018

# CsCl from Chliatzou et al (2018) p. 8, Tab. 5, Eq. (1)
coef = {
    'c0': 208.8,
    'c1': -0.115,
    'Tm': 919.15,
    }

lambda_ = Chliatzouetal2018(1000.0, coef=coef)

def test():
    assert lambda_*1000.0 == approx(199.50225, 0.00005)
