import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import Assaeletal2017

# In from metals.yml
coef = {
    'c1': 36.493,
    'c2': 0.029185,
    'Tm': 429.748,
    }

lambda_ = Assaeletal2017(600.0, coef=coef)

def test():
    assert lambda_ == approx(41.462, 0.00005)
