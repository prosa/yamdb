import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import Touloukian1970b

# Mg from Iida, Guthrie (2015)
coef = {
    'a': 19.7907,
    'b': 0.0631304,
    }

lambda_ = Touloukian1970b(1000.0, coef=coef)

def test():
    assert lambda_ == approx(82.921, 0.00005)
