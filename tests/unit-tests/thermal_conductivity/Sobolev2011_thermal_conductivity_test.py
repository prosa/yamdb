import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import Sobolev2011

# K from Ohse (1985)
coef = {
    'a': 75.5685,
    'b': -0.0587519,
    'c': 1.40155e-05,
    }

lambda_ = Sobolev2011(500.0, coef=coef)

def test():
    assert lambda_ == approx(49.696425, 0.00005)
