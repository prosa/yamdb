import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.thermal_conductivity import SquareRoot

# Li from Iida, Guthrie (2015)
coef = {
    'a': -30.6834,
    'b': -0.07775,
    'c': 5.27445,
    }

lambda_ = SquareRoot(500.0, coef=coef)

def test():
    assert lambda_ == approx(48.382, 0.00005)
