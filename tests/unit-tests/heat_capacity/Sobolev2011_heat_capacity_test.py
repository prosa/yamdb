import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import Sobolev2011

# Pb from metals.yml
coef = {
    'cp_0': 176.2,
    'a': -4.923E-02,
    'b': 1.544E-05,
    'c': -1.524E+06,
}

cp = Sobolev2011(1000.0, coef=coef)

def test():
    assert cp == approx(140.89, 0.00005)
