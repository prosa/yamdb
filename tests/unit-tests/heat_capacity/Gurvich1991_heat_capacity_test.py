import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import Gurvich1991

# Pb from metals.yml
coef = {
    'cp_0': 175.1,
    'a': -4.961E-02,
    'b': 1.985E-05,
    'c': -2.099E-09,
    'd': -1.524E+06,
    }

cp = Gurvich1991(1000.0, coef=coef)

def test():
    assert cp == approx(141.72, 0.00005)
