import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import IidaGuthrie2015

# Bi from metals.yml
coef = {
      'cp_0': 19.04,
      'a': 10.38E-03,
      'b': -3.97E-06,
      'c': 20.75E+05,
      'M': 208.9804E-03,
}

cp = IidaGuthrie2015(1000.0, coef=coef)

def test():
    assert cp == approx(131.71, 0.00005)
