import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import Imbeni1998

# Bi from metals.yml
coef = {
    'cp_0': 118.2,
    'a': 5.934E-03,
    'b': 7.183E+06,
}

cp = Imbeni1998(1000.0, coef=coef)

def test():
    assert cp == approx(131.32, 0.00005)
