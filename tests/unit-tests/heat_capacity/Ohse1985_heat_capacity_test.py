import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import Ohse1985

# Cs from metals.yml
coef = {
    'C1': 36.519,
    'C2': -2.1139E-02,
    'C3': 1.5891E-05,
    'M': 132.90543E-03,
}

cp = Ohse1985(800.0, coef=coef)

def test():
    assert cp == approx(224.05, 0.00005)
