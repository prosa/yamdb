import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import IAEA2008

# Hg from metals.yml
coef = {
    'cp_0': 0.1508,
    'a': -6.630E-05,
    'b': 6.4185E-08,
    'c': 0.8049,
    }

cp = IAEA2008(400.0, coef=coef)

def test():
    assert cp == approx(136.56, 0.00005)
