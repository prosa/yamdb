import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.heat_capacity import IidaGuthrie1988

# Mg from metals.yml
coef = {
    'cp_mol': 32.64,
    'M': 24.305E-03,
    }

cp = IidaGuthrie1988(1000.0, coef=coef)

def test():
    assert cp == approx(1342.9, 0.00005)
