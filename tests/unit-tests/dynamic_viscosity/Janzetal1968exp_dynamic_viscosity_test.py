import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Janzetal1968exp

# KI p. 76, Tab. 86 Janz et al (1968)
coef = {
    'A': 9.836E-02,
    'E': 5343.0,
    }

eta = Janzetal1968exp(1000.0, coef=coef)


def test():
    assert eta*1000.0 == approx(1.45, 0.002)
