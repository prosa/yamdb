import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import ToerklepOeye1982

# MgCl2 Toerklep, Oeye (1982) p. 388 Eq. 6
coef = {
    'A': 0.17985,
    'B': 2470.1,
    'C': 0,
    }

eta = ToerklepOeye1982(1000.0, coef=coef)


def test():
    assert eta*1000.0 == approx(2.126479704, 0.000000001)
