import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Janzetal1968

# CsI p. 77, Tab. 88 Janz et al (1968)
coef = {
    'a': 41.8211,
    'b': -0.101156,
    'c': 8.49570E-05,
    'd': -2.42543E-08,
    }

eta = Janzetal1968(1000.0, coef=coef)


def test():
    assert eta*1000.0 == approx(1.37, 0.002)
