import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Assaeletal2012

# Sn from metals.yml
coef = {
    'a1': 0.408,
    'a2': 343.4,
    }

eta = Assaeletal2012(1000.0, coef=coef)


def test():
    assert eta == approx(0.00086179, 0.000005)
