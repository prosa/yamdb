import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992plusE

# KNO3 Tasidou et al (2019) p. 10, Tab. 6
coef = {
    'R': 8.314459848,
    'D1': 0.0840,
    'D2': 17994.1,
    }

eta = Linstrom1992plusE(800.0, coef=coef)


def test():
    assert eta*1000.0 == approx(1.26, 0.005)
