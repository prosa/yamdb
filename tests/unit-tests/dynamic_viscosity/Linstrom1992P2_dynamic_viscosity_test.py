import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992P2

# NaCl-NaO3 8-92 p. 293 Tab. 2.4.a Janz (1988)
coef = {
    'D1': 141.203,
    'D2': -0.403,
    'D3': 0.00029137,
    }

eta = Linstrom1992P2(600.0, coef=coef)


def test():
    assert eta*1000.0 == approx(4.2962, 0.0000000001)
