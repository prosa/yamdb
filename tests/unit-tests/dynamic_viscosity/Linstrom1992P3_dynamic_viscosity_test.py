import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992P3

# KNO3-NaO3 50-50 not found in Janz (1988) -> calculated from coefficients
coef = {
    'D1': 74.249,
    'D2': -0.2745,
    'D3': 0.00034714,
    'D4': -1.475e-07,
    }

eta = Linstrom1992P3(600.0, coef=coef)


def test():
    assert eta*1000.0 == approx(2.6594, 0.0000000001)
