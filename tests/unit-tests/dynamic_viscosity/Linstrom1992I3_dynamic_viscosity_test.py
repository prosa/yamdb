import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992I3

# CsBr-CsF from Janz 1988
# x_CsBr = 0.0 - 1.0 (range)
# x_CsBr = 0.3 -> 30 mol%
# Janz et al (1988) p. 279, Tab. 2.4.a
coef = {
    'D1': 1.304,
    'D2': -0.00263,
    'D3': -4.083e-05,
    'D4': 5.694e-07,
    'Tmax': 1070.0,
    'Tmin': 1070.0,
    'component': 'first',
}

eta = Linstrom1992I3(1070.0, 30, coef=coef)

def test():
    assert eta*1000.0  == approx(1.2037268, 0.000000001)
