import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Hirai1992

# Zn from metals.yml
coef = {
    'A': 0.5266,
    'B': 10.91,
    }

eta = Hirai1992(1000.0, coef=coef)


def test():
    assert eta == approx(0.0019559, 0.0001)
