import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992E2

# Sulfide Na2S3.1 p. 297 Tab. 2.4.a Janz (1988)
coef = {
    'D1': 0.5624,
    'D2': 8443.45,
    'D3': 332.0,
    }

eta = Linstrom1992E2(600.0, coef=coef)


def test():
    assert eta*1000.0 == approx(24.87127234, 0.000005)
