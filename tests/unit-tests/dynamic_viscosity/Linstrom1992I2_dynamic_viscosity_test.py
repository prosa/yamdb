import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992I2

# CsBr-CsI from Janz 1988
# x_CsBr = 0.0 - 1.0 (range)
# x_CsBr = 0.3 -> CsI = 0.7 -> 70 mol%
# Janz et al (1988) p. 279, Tab. 2.4.a
coef = {
    'D1': 1.203,
    'D2': -0.0023,
    'D3': 4.253e-06,
    'Tmax': 1070.0,
    'Tmin': 1070.0,
    'component': 'second',
}

eta = Linstrom1992I2(1070.0, 30, coef=coef)

def test():
    assert eta*1000.0  == approx(1.0628397, 0.000000001)
