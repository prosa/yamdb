import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import KostalMalek2010

# Se  Kostal, Malek (2010)
coef = {
    'A': -9.27,
    'B': 2795,
    'T0': 226.1,
    }

eta = KostalMalek2010(600.0, coef=coef)


def test():
    assert eta == approx(0.16617, 0.00001)
