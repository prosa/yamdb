import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Linstrom1992I4

# CsCl-CsF from Janz 1988
# x_CsCl = 0.0 - 1.0 (range)
# x_CsCl = 0.3 -> 30 mol%
# Janz et al (1988) p. 279, Tab. 2.4.a
coef = {
    'D1': 1.304,
    'D2': -0.01404,
    'D3': 0.0003029,
    'D4': -2.709e-06,
    'D5': 8.148e-09,
    'Tmax': 1070.0,
    'Tmin': 1070.0,
    'component': 'first',
}

eta = Linstrom1992I4(1070.0, 30, coef=coef)

def test():
    assert eta*1000.0  == approx(1.08886688, 0.000000001)
