import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import Ohse1985

# Cs Ohse (1985)
coef = {
    'a': -6.4072,
    'b': -0.40767,
    'c': 432.75,
    }

eta = Ohse1985(600.0, coef=coef)


def test():
    assert eta == approx(0.00025007, 0.0001)
