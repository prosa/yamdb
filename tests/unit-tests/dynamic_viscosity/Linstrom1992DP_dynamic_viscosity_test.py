import sys
sys.path.insert(0, '../../../')

from yamdb.properties.dynamic_viscosity import Linstrom1992DP

# MgCl2-NaCl Janz (1988) p. 292, Tab. 2.4.a
coef = {
    'D1': 1.2,
    }

eta = Linstrom1992DP(1073.0, coef=coef)


def test():
    assert eta*1000.0 == 1.2
