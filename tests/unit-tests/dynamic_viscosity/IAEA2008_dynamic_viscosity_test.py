import sys
sys.path.insert(0, '../../../')
from pytest import approx

from yamdb.properties.dynamic_viscosity import IAEA2008

# Ba from metals.yml (Sphilrain et al. 1980)
coef = {
    'a': 0.215E-03,
    'b': 0,
    'c': 2098,
    }

eta = IAEA2008(1200.0, coef=coef)


def test():
    assert eta == approx(0.0012352, 0.00005)
