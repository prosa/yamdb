import sys
sys.path.insert(0, '../')

import yamdb.yamref as yrf
import yamdb.yamdb as ydb


def test():
    properties = [
        'density',
        'dynamic_viscosity',
        'expansion_coefficient',
        'heat_capacity',
        'resistivity',
        'sound_velocity',
        'surface_tension',
        'thermal_conductivity',
        'vapor_pressure',
        ]

    undefined = []
    
    ldb = yrf.BibTexDB('../yamdb/data/references.bib')
    sdb = ydb.SubstanceDB('../yamdb/data/salts.yml')

    p = {}

    for material in sdb.materials.keys():
        p[material] = ydb.Properties(sdb.get_substance(material))
        for property_ in properties:
            if property_ in p[material].get_property_list():
                slist = p[material].get_source_list(property_)
                for source in slist:
                    if not ldb.get_entry(p[material].get_reference(property_, source)):
                        undefined.append('missing entry for: %s %s %s' % (material, property_, source))

    assert undefined == []
