import yaml
import bibtexparser
from bibtexparser.customization import convert_to_unicode
from bibtexparser.bparser import BibTexParser

def test():
    missing = []

    bibf = '../yamdb/data/references.bib'
    ymlf = '../yamdb/data/references.yml'

    with open(ymlf) as fp:
        ydb = yaml.load(fp, yaml.SafeLoader)

    with open(bibf) as bibtex_file:
                parser = BibTexParser()
                parser.customization = convert_to_unicode
                bdb = bibtexparser.load(bibtex_file, parser=parser)

    for key in bdb.get_entry_dict().keys():
        if key not in ydb.keys():
            missing.append('%s missing from YAML database' % key)

    for key in ydb.keys():
        if key not in bdb.get_entry_dict().keys():
            missing.append('%s missing from BibTeX database' % key)
    
    assert missing == []
