#!/usr/bin/env bash
keylist=`sed -ne '/@/p' < $1 | sed -e 's/@.*{//g' | sed -e 's/,$//g' | sort -`
for key in ${keylist}
do
    citekeylist=${citekeylist}" "cite:${key}
done
# a style that does not sort on itself is needed to keep
# ${keylist} and ${references} in sync: eh_unsrt.csl
# from
# http://www.zotero.org/styles/elsevier-harvard
# with everything between <sort> and </sort> (multiple times)
# removed
# https://stackoverflow.com/questions/28902783/how-does-pandoc-citeproc-sort-citations
references=`echo $citekeylist | pandoc - --wrap=none --from org --citeproc --csl eh_unsrt.csl --bibliography $1 --to=plain -o -`
keylist=($keylist)
# get rid of the citation keys (first line)
IFS=$'\n'
references=(`echo -n "${references}" | awk -v RS= '{if (NR > 1) {print $0}}'`)
echo "---"
for ((i=0;i<${#keylist[@]};i++))
do
    echo ${keylist[$i]}:
    echo -n \"${references[$i]}\" | sed -e 's/^/  /g' | fmt
done
echo "..."
