"""Implements the surface_tension methods."""
import numpy as np
coef = None


def IAEA2008(Temp, coef=coef):
    r"""Return surface tension according to IAEA (2008), p. 87, eq. (3.33).

    .. math:: \sigma = a + b*(T + c)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    sigma = a + b*(Temp + c)
    sigma /= 1000.  # mN -> N
    return sigma


def Sobolev2011(Temp, coef=coef):
    r"""Return surface tension according to OECDNEA (2015), p. 67, eq. (2.26).

    .. math:: \sigma = (a + b*T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    sigma = (a + b*Temp)
    sigma /= 1000.  # mN -> N
    return sigma


def Linstrom1992DP(Temp, coef=coef):
    r"""Return the surface tension according to Linstrom (1992) eq. DP.

    .. math:: \sigma = D1

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    sigma = D1/1000.0  # mN -> N
    return sigma


def Linstrom1992P1(Temp, coef=coef):
    r"""Return the surface tension according to Linstrom (1992), eq. P1.

    .. math:: \sigma = D1 + D2 * T

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    sigma = D1 + D2*Temp
    sigma /= 1000.0  # mN -> N
    return sigma


def Linstrom1992P2(Temp, coef=coef):
    r"""Return the surface tension according to Linstrom (1992), eq. P2.

    .. math:: \sigma = D1 + D2 * T + D3 * T^2

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    sigma = D1 + D2*Temp + D3*np.power(Temp, 2)
    sigma /= 1000.0  # mN -> N
    return sigma


def Linstrom1992I1(Temp, *args, coef=coef):
    r"""Return the surface tension according to Linstrom (1992) eq. I1.

    .. math:: \sigma = D1 + D2 C

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    sigma = D1 + D2*C
    sigma /= 1000.0  # mN -> N
    return sigma


def Linstrom1992I2(Temp, *args, coef=coef):
    r"""Return the surface tension according to Linstrom (1992) eq. I2.

    .. math:: \sigma = D1 + D2 C + D3 C^2

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    sigma = D1 + D2*C + D3*np.power(C, 2)
    sigma /= 1000.0  # mN -> N
    return sigma


def Linstrom1992I3(Temp, *args, coef=coef):
    r"""Return the surface tension according to Linstrom (1992) eq. I3.

    .. math:: \sigma = D1 + D2 C + D3 C^2 + D4 C^3

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    sigma = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3)
    sigma /= 1000.0  # mN -> N
    return sigma


def Linstrom1992I4(Temp, *args, coef=coef):
    r"""Return the surface tension according to Linstrom (1992) eq. I4.

    .. math:: \sigma = D1 + D2 C + D3 C^2 + D4 C^3 + D5 C^4

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Surface tension in N/m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        D5 = coef['D5']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    sigma = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3) + D5*np.power(C, 4)
    sigma /= 1000.0  # mN -> N
    return sigma
