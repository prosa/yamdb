"""Implements the vapour_pressure methods."""
import numpy as np

coef = None


def IAEA2008(Temp, coef):
    r"""Return vapour pressure according to IAEA (2008).

    p. 87, eq. (3.36)

    .. math:: \ln(pv) = a + b/T + c*\ln(T) + d

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Vapour pressure in Pa, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    pv = a + b/Temp + c*np.log(Temp) + d*Temp
    pv = np.exp(pv)
    return pv


def IidaGuthrie2015(Temp, coef):
    r"""Return vapour pressure according to Iida, Guthrie (2015).

    p. 514ff, Tab. 17.5

    .. math:: \log(pv) = A + B/T + C*\log(T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Vapour pressure in Pa, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 2015. The thermophysical properties of
    metallic liquids. Oxford University Press, Oxford.

    """
    try:
        A = coef['A']
        B = coef['B']
        C = coef['C']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'unit' in coef:
        unit = coef['unit']
    else:
        unit = 'atm'
    pv = A + B/Temp + C*np.log10(Temp)
    pv = np.power(10.0, pv)
    if unit == 'mmHg':
        pv *= 133.322  # mmHg -> Pa
    elif unit == 'Pa':
        pass
    else:
        pv *= 101325.0  # atm -> Pa
    return pv

def Kelley1935(Temp, coef):
    r"""Return vapour pressure according to Kelley (1935).

    based on the equations given for the standard free-energy change
    (p. 2, Eq. (2), p. 10, vapour pressure may be computed from ...)
    
    .. math:: \Delta F° = - R T \ln(pv)
    .. math:: \Delta F° = A + B * T * \log10(T) - C * T
    .. math:: pv = \exp(- \Delta F° / (R * T))

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Vapour pressure in Pa, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Kelley, K.K., 1935. Contributions to the data on theoretical metallurgy
    III. The free energies of vaporization and vapor pressures of inorganic
    substances (Bulletin No. 383). Bureau of Mines, USDI, Washington, D. C.
    """
    try:
        A = coef['A']
        B = coef['B']
        C = coef['C']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    R = 1.9869  # cal/K
    deltaF = A + B*Temp*np.log10(Temp) - C*Temp
    pv = np.exp(- deltaF/(R*Temp))
    pv *= 101325.0  # atm -> Pa
    return pv

def Ohse1985(Temp, coef):
    r"""Return vapour pressure according to Ohse (1985).

    p. 355, Tab. 6

    .. math:: \ln(pv) = A + B/T + C*\ln(T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Vapour pressure in Pa, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Ohse, R.W. (Ed.), 1985. Handbook of thermodynamic and transport
    properties of alkali metals, International union of pure and applied
    chemistry chemical data series. Blackwell Scientific Publications,
    Oxford, London, Edinburgh, Boston, Palo Alto, Melbourne.

    """
    try:
        A = coef['A']
        B = coef['B']
        C = coef['C']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    pv = A + B/Temp + C*np.log(Temp)
    pv = np.exp(pv)
    pv *= 1E06  # MPa -> Pa
    return pv


def Sobolev2011(Temp, coef):
    r"""Return vapour pressure according to OECDNEA (2015).

    p. 53, eq. (2.17)

    pv = a*exp(b/T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Vapour pressure in Pa, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    pv = a * np.exp(b/Temp)
    return pv
