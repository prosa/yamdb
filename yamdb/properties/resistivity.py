"""Implements the resistivity methods."""
import numpy as np

coef = None


def Baker1968(Temp, coef=coef):
    r"""Return resistivity according to Baker (1968), p. 1091, eq. (2).

    .. math:: \log(\rho) = A/T - B

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Baker, E.H., 1968. The vapour pressure and resistivity of selenium at
    high temperatures. Journal of the Chemical Society A: Inorganic,
    Physical, Theoretical 1089–1092.
    
    """
    try:
        A = coef['A']
        B = coef['B']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    logrho_e = A/Temp - B
    rho_e = np.power(10.0, logrho_e)
    rho_e /= 100.0  # Ohm cm -> Ohm m
    return rho_e


def constant(Temp, coef=coef):
    r"""Return constant resistivity according to Iida, Guthrie (1988), p. 232.

    rho_e = value

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    """
    try:
        value = coef['value']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    # just to return vector if required:
    return value * Temp / Temp


def CusackEnderby1960(Temp, coef=coef):
    r"""Return resistivity according to Iida, Guthrie (1988), p. 232.

    see Cusack, Enderby (1960)

    .. math:: \rho_e = \alpha * T + \beta

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Cusack, N., Enderby, J.E., 1960. A note on the resistivity of liquid
    alkali and noble metals. Proceedings of the Physical Society 75,
    395–401.

    """
    try:
        alpha = coef['alpha']
        beta = coef['beta']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return alpha * Temp + beta


def Desaietal1984(Temp, coef=coef):
    r"""Return resistivity according to Desai et al. (1984).

    .. math:: \rho_e/ 10^{-8} \Omega m = a + b*T + c*T^2 + d*T^3

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Desai, P.D., Chu, T.K., James, H.M., Ho, C.Y., 1984. Electrical
    resistivity of selected elements. Journal of Physical and Chemical
    Reference Data 13, 1069–1096.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho_e = a + b*Temp + c*np.power(Temp, 2) + d*np.power(Temp, 3)
    rho_e *= 1.E-08
    return rho_e


def fractionalNegativeExponent(Temp, coef=coef):
    r"""Return resistivity according to the fit below.

    .. math:: \rho = a + b/x^c

    gave the best fit for the Te resistivity data from
    Iida Guthrie (2015) p. 541, Tab. 17.11
    with

    | a = 3.76373e-06
    | b = 6.55205e+12
    | c = 6.5

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho_e = a + b/np.power(Temp, c)
    return rho_e


def IAEA2008(Temp, coef=coef):
    r"""Return resistivity according to IAEA (2008).

    .. math:: \rho_e/\rho_{e0} = a + b*t + c*t^2 + d*t^3 + e*t^4 + f*t^5

    t in °C !

    the 10E08 Ohm m in IAEA eq. (3.35) p. 87 is obviously
    in error

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
        e = coef['e']
        f = coef['f']
        rho_e0 = coef['rho_e0']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    t = Temp - 273.15
    rho_e = (a + b*t + c*np.power(t, 2) + d*np.power(t, 3) +
             e*np.power(t, 4) + f*np.power(t, 5))
    rho_e *= rho_e0
    # rho_e *= 1.E-08
    return rho_e


def Janzetal1968(Temp, coef=coef):
    r"""Return the resistivity according to Janz et al. (1968).

    Calculated from the conductivity equation (kappa in
    1/(Ohm cm)) for specific conductance p. 1 with polynomial
    containing all coefficients

    .. math:: \kappa = a + b*T + c*T^2 + d*T^3

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., Dampier, F.W., Lakshminarayanan, G.R., Lorenz, P.K.,
    Tomkins, R.P.T., 1968. Molten salts (No. 15), Nat. Stand. Ref. Data ser.
    National Bureau of Standards, Washington.

    """
    c = 0.0
    d = 0.0
    Tref = 0.0
    try:
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'c' in coef.keys():
        c = coef['c']
    if 'd' in coef.keys():
        d = coef['d']
    if 'Tref' in coef.keys():
        Tref = coef['Tref']
    kappa = a + b*(Temp - Tref) + c*np.power(Temp - Tref, 2) + \
        d*np.power(Temp - Tref, 3)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Janz1967exp(Temp, coef=coef):
    r"""Return the resistivity according to Janz (1967) p. 290.

    .. math:: \kappa = A \exp(-E/R T)

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1967. Molten salts handbook. Academic Press, New York.
    """
    R = 1.98717E-03  # kcal mol^-1 K^-1
    try:
        A = coef['A']
        E = coef['E']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = A*np.exp(-E/(R*Temp))
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992plusE(Temp, coef=coef):
    r"""Return the resistivity according to Janz (1992) eq. E+.

    .. math:: \kappa = D1 \exp(D2/(8.31441 T))

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    R = 8.31441
    try:
        D1 = coef['D1']
        D2 = coef['D2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = D1*np.exp(D2/(R*Temp))
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992DP(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. DP.

    .. math:: \kappa = D1

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = D1 * 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992E2(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. E2.

    .. math:: \kappa = D1 \exp(D2/(R * (T - D3)))

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    R = 8.31441
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = D1*np.exp(D2/(R*(Temp - D3)))
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992P1(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. P1.

    .. math:: \kappa = D1 + D2 * T

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = D1 + D2*Temp
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992P2(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. P2.

    .. math:: \kappa = D1 + D2 * T + D3 * T^2

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    kappa = D1 + D2*Temp + D3*np.power(Temp, 2)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992P3(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. P3.

    .. math:: \kappa = D1 + D2 * T + D3 * T^2 + D4 * T^3

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    kappa = D1 + D2*Temp + D3*np.power(Temp, 2) + D4*np.power(Temp, 3)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992P4(Temp, coef=coef):
    r"""Return the resistivity according to Linstrom (1992), eq. P4.

    .. math:: \kappa = D1 + D2 * T + D3 * T^2 + D4 * T^3 + D5 * T^4

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        D5 = coef['D5']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = D1 + D2*Temp + D3*np.power(Temp, 2) + D4*np.power(Temp, 3) + D5*np.power(Temp, 4)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992I1(Temp, *args, coef=coef):
    r"""Return the resistivity according to Linstrom (1992) eq. I1.

    .. math:: \kappa = D1 + D2 C

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    kappa = D1 + D2*C
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992I2(Temp, *args, coef=coef):
    r"""Return the resistivity according to Linstrom (1992) eq. I2.

    .. math:: \kappa = D1 + D2 C + D3 C^2

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    kappa = D1 + D2*C + D3*np.power(C, 2)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992I3(Temp, *args, coef=coef):
    r"""Return the resistivity according to Linstrom (1992) eq. I3.

    .. math:: \kappa = D1 + D2 C + D3 C^2 + D4 C^3

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    kappa = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Linstrom1992I4(Temp, *args, coef=coef):
    r"""Return the resistivity according to Linstrom (1992) eq. I4.

    .. math:: \kappa = D1 + D2 C + D3 C^2 + D4 C^3 + D5 C^4

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        D5 = coef['D5']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    C = x
    if component == "second":
        C = 100.0 - C
    kappa = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3) + D5*np.power(C, 4)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Massetetal2006(Temp, coef=coef):
    r"""Return the resistivity according to Masset et al. (2006) p. 755 Tab. 2.

    .. math:: \kappa = A \exp(-E/T)

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Masset, P., Henry, A., Poinso, J.-Y., Poignet, J.-C., 2006. Ionic
    conductivity measurements of molten iodide-based electrolytes. Journal
    of Power Sources 160, 752–757.
    """
    try:
        A = coef['A']
        E = coef['E']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    kappa = A*np.exp(E/Temp)
    kappa *= 100.0  # 1/(Ohm cm) -> 1/(Ohm m)
    return 1.0/kappa


def Ohse1985(Temp, coef=coef):
    r"""Return resistivity according to Ohse (1985), p. 730, eq. (36).

    .. math:: \log(\rho_e) = a + b * \log(T/Tmin) + c * \log^2(T/Tmin) + d * \log^3(T/Tmin)

    :math:`\rho_e` is in 10E-08 Ohm m

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Ohse, R.W. (Ed.), 1985. Handbook of thermodynamic and transport
    properties of alkali metals, International union of pure and applied
    chemistry chemical data series. Blackwell Scientific Publications,
    Oxford, London, Edinburgh, Boston, Palo Alto, Melbourne.

    """
    # we have to treat scalars and arrays separately because of
    # "TypeError: iteration over a 0-d array" if np.array(Temp) has
    # only one element
    try:
        n_sect = len(coef['range'])
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    if isinstance(Temp, np.ndarray):
        idx = np.zeros_like(Temp, dtype=int)
        a = np.zeros_like(Temp)
        b = np.zeros_like(Temp)
        c = np.zeros_like(Temp)
        d = np.zeros_like(Temp)
        Tmin = np.zeros_like(Temp)
        rho_e = np.zeros_like(Temp)
        #
        #
        #
        for i in range(n_sect - 1, -1, -1):
            try:
                Tmax = coef['range'][i]['Tmax']
            except LookupError as e:
                print("ERROR: Not all coefficients supplied, cannot compute values!\n"
                      "First missing coefficient: %s" % e)
                raise                
            idx[Temp < Tmax] = i
        for i in range(n_sect):
            try:
                Tmin[idx == i] = coef['range'][i]['Tmin']
                a[idx == i] = coef['range'][i]['a']
                b[idx == i] = coef['range'][i]['b']
                c[idx == i] = coef['range'][i]['c']
                d[idx == i] = coef['range'][i]['d']
            except LookupError as e:
                print("ERROR: Not all coefficients supplied, cannot compute values!\n"
                      "First missing coefficient: %s" % e)
                raise        
            rho_e[idx == i] = Ohse1985_func(
                Temp[idx == i],
                a[idx == i],
                b[idx == i],
                c[idx == i],
                d[idx == i],
                Tmin[idx == i])
    else:  # isinstance
        idx = n_sect - 1
        for i in range(n_sect - 1, -1, -1):
            try:
                Tmax = coef['range'][i]['Tmax']
            except LookupError as e:
                print("ERROR: Not all coefficients supplied, cannot compute values!\n"
                      "First missing coefficient: %s" % e)
                raise
            if Temp < Tmax:
                idx = i
        try:
            Tmin = coef['range'][idx]['Tmin']
            a = coef['range'][idx]['a']
            b = coef['range'][idx]['b']
            c = coef['range'][idx]['c']
            d = coef['range'][idx]['d']
        except LookupError as e:
            print("ERROR: Not all coefficients supplied, cannot compute values!\n"
                  "First missing coefficient: %s" % e)
            raise            
        rho_e = Ohse1985_func(Temp, a, b, c, d, Tmin)
    rho_e *= 1E-08
    return rho_e


def Ohse1985_func(T, a, b, c, d, Tmin):
    """Helper function to calculate the electrical resistivity.

    According to Ohse (1985)

    Parameters
    ----------
    T : float
        temperature (array or single value)
    a : float
        coefficient a
    b : float
        coefficient b
    c : float
        coefficient c
    d : float
        coefficient d
    Tmin : float
        minimum Temperature

    Returns
    -------
    float
        resistivity (array or single value)

    References
    ----------
    Ohse, R.W. (Ed.), 1985. Handbook of thermodynamic and transport
    properties of alkali metals, International union of pure and applied
    chemistry chemical data series. Blackwell Scientific Publications,
    Oxford, London, Edinburgh, Boston, Palo Alto, Melbourne.

    """
    logT = np.log10(T/Tmin)
    logrho_e = a + b*logT + c*np.power(logT, 2) + d*np.power(logT, 3)
    rho_e = np.power(10.0, logrho_e)
    return rho_e


def SalyulevPotapov2015(Temp, coef=coef):
    r"""Return resistivity according to Salyulev, Potapov (2015).

    p. 486 eq. (2).

    .. math:: log(\kappa) = a + b/T + c/T^2 + d/T^3

    :math:`\kappa` is the conductivity in S/cm

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Salyulev, A.B., Potapov, A.M., 2015. Conductivity of some molten
    chlorides at elevated temperatures i. Experimental and calculation
    techniques for BeCl2, ZnCl2, and PbCl2SalyulevPotapov2015. Journal of
    Chemical & Engineering Data 60, 484–492.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    logkappa = a + b/Temp + c/np.power(Temp, 2) + d/np.power(Temp, 3)
    kappa = np.power(10.0, logkappa)
    kappa *= 100.0  # S/cm -> S/m
    return 1.0/kappa


def Sobolev2011(Temp, coef=coef):
    r"""Return resistivity according to Sobolev (2011).

    p. 156 Sobolev (2011)

    .. math:: \rho_e/(Ohm m) = a + b*T + c*T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    rho_e = a + b*Temp + c*Temp*Temp
    return rho_e


def Zinkle1998(Temp, coef=coef):
    r"""Return resistivity according to Ohse (1985), p. 2 Zinkle (1998).

    .. math:: \rho_e/(nOhm m) = a + b*T + c*T^2 + d*T^3 + e*T^4

    nano Ohm m !

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Electrical resistivity in Ohm m, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Zinkle, S.J., 1998. Summary of physical properties for lithium, Pb-17Li,
    and (LiF)n.BeF2 coolants.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
        e = coef['e']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise        
    rho_e = (a + b*Temp + c*np.power(Temp, 2) + d*np.power(Temp, 3) +
             e*np.power(Temp, 4))
    rho_e *= 1.E-09  # nano Ohm m
    return rho_e
