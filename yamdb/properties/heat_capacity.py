"""Implements the heat_capacity methods."""
import numpy as np

coef = None


def Gurvich1991(Temp, coef=coef):
    r"""Return heat capacity according to OECDNEA (2015) p. 91, eq. (2.47).

    .. math:: cp = cp_0 + a*T + b*T^2 + c*T^3 + d/T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    OECD Nuclear Energy Agency, Nuclear Science Committee, 2015. Handbook on
    lead-bismuth eutectic alloy and lead properties, materials
    compatibility, thermal-hydraulics and technologies. OECD/NEA/NSC.

    """
    try:
        cp_0 = coef['cp_0']
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    cp = (cp_0 + a*Temp + b*np.power(Temp, 2) +
          c*np.power(Temp, 3) + d/np.power(Temp, 2))
    return cp


def IAEA2008(Temp, coef=coef):
    r"""Return heat capacity according to IAEA (2008), p. 87, eq. (3.30).

    .. math:: cp = cp_0 + a*T + b*T^2 + c/T

    value probably in kJ Kg^-1 K^-1 and _not_ in J Kg^-1 K^-1
    cf. p. 83 Tab. 3.11

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        cp_0 = coef['cp_0']
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    cp = cp_0 + a*Temp + b*np.power(Temp, 2) + c/Temp
    return cp * 1000.0  # kJ -> J


def IidaGuthrie1988(Temp, coef=coef):
    r"""Return heat capacity according to Iida Guthrie (1988), p. 91, Tab. 4.3.

    cp_molar (J mol^-1 K^-1) = constant

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 1988. The physical properties of liquid
    metals. Clarendon Press, Oxford.

    """
    try:
        cp_mol = coef['cp_mol']
        M = coef['M']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'energy_unit' in coef:
        energy_unit = coef['energy_unit']
    else:
        energy_unit = 'J'
    if energy_unit == 'cal':
        cp_mol *= 4.184  # cal -> J according to Janz (1979) p. 4
    cp = cp_mol / M  # M assumed to be in kg/mol
    # just to return vector if required:
    return cp * Temp / Temp


def IidaGuthrie2015(Temp, coef=coef):
    r"""Return heat capacity according to IidaGuthrie (2015), p. 524.

    unified for all equation types

    cp_molar (J mol^-1 K^-1) = :math:`cp_0 + a*T + b*T^2 + c*T^{-2}`

    set missing coefficients to 0 in yml database

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 2015. The thermophysical properties of
    metallic liquids. Oxford University Press, Oxford.

    """
    try:
        cp_0 = coef['cp_0']
        a = coef['a']
        b = coef['b']
        c = coef['c']
        M = coef['M']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'energy_unit' in coef:
        energy_unit = coef['energy_unit']
    else:
        energy_unit = 'J'
    # float conversion, otherwise: "ValueError: Integers to negative
    # integer powers are not allowed." -> bails out with
    # TypeError: only size-1 arrays can be converted to Python scalars
    # -> np.float_power
    cp_mol = cp_0 + a*Temp + b*np.power(Temp, 2) + c*np.float_power(Temp, -2)
    if energy_unit == 'cal':
        cp_mol *= 4.184  # cal -> J according to Janz (1979) p. 4
    cp = cp_mol / M  # M assumed to be in kg/mol
    return cp


def Imbeni1998(Temp, coef=coef):
    r"""Return heat capacity according to OECDNEA (2015), p. 93, eq. (2.49).

    .. math:: cp = cp_0 + a*T + b/T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    OECD Nuclear Energy Agency, Nuclear Science Committee, 2015. Handbook on
    lead-bismuth eutectic alloy and lead properties, materials
    compatibility, thermal-hydraulics and technologies. OECD/NEA/NSC.

    """
    try:
        cp_0 = coef['cp_0']
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    cp = cp_0 + a*Temp + b/np.power(Temp, 2)
    return cp


def Ohse1985(Temp, coef=coef):
    r"""Return heat capacity according to Ohse (1985).

    p. 429, eq. (17), Tab. 14

    cp/(J mol^-1 K^-1) = :math:`C1 + C2*T + C3*T^2`

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Ohse, R.W. (Ed.), 1985. Handbook of thermodynamic and transport
    properties of alkali metals, International union of pure and applied
    chemistry chemical data series. Blackwell Scientific Publications,
    Oxford, London, Edinburgh, Boston, Palo Alto, Melbourne.

    """
    try:
        C1 = coef['C1']
        C2 = coef['C2']
        C3 = coef['C3']
        M = coef['M']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    cp_mol = C1 + C2*Temp + C3*np.power(Temp, 2)
    cp = cp_mol / M  # M assumed to be in kg/mol
    return cp


def Sobolev2011(Temp, coef=coef):
    r"""Return heat capacity according to OECDNEA (2015), p. 91, eq. (2.48).

    .. math:: cp = cp_0 + a*T + b*T^2 + c/T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Heat capacity in J/(Kg K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        cp_0 = coef['cp_0']
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    cp = cp_0 + a*Temp + b*np.power(Temp, 2) + c/np.power(Temp, 2)
    return cp
