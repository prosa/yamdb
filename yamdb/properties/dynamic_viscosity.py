"""Implements the dynamic_viscosity methods."""
import numpy as np

coef = None


def Assaeletal2012(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Assael et al. (2012).

    p. 13, eq. (2), tab. 5.

    .. math:: log10(\eta /(m Pa s)) = -a1 + a2/T

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Assael, M.J., Armyra, I.J., Brillo, J., Stankus, S.V., Wu, J., Wakeham,
    W.A., 2012. Reference data for the density and viscosity of liquid
    cadmium, cobalt, gallium, indium, mercury, silicon, thallium, and zinc.
    Journal of Physical and Chemical Reference Data 41, 033101.

    """
    try:
        a1 = coef['a1']
        a2 = coef['a2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise   
    eta = -a1 + a2/Temp
    eta = np.power(10.0, eta)
    eta /= 1000.0
    return eta


def Hirai1992(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Hirai (1992) p. 401/63, Tab. 1.

    .. math:: \eta = A \exp(B/R T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Hirai, M., 1992. Estimation of viscosities of liquid alloys.
    Tetsu-to-Hagane 78, 399–406.

    """
    R = 8.3144  # J mol^-1 K^-1 (gas constant Hirai (1992))
    try:
        A = coef['A']
        B = coef['B']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise   
    eta = A*np.exp(B*1000.0/(R*Temp))  # kJ -> J in B
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def IAEA2008(Temp, coef=coef):
    r"""Return the dynamic viscosity according to IAEA (2008) p. 87, eq. (3.32).

    .. math:: \eta = a * T^b * \exp(c/T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise   
    eta = a * np.power(Temp, b) * np.exp(c/Temp)
    return eta


def Janzetal1968(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Janz et al. (1968).

    Calculated from polynomial containing all
    coefficients (eta in cP) on p. 1

    .. math:: \eta = a + b*T + c*T^2 + d*T^3

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., Dampier, F.W., Lakshminarayanan, G.R., Lorenz, P.K.,
    Tomkins, R.P.T., 1968. Molten salts (No. 15), Nat. Stand. Ref. Data Ser.
    National Bureau of Standards, Washington.

    """
    c = 0.0
    d = 0.0
    try:
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'c' in coef.keys():
        c = coef['c']
    if 'd' in coef.keys():
        d = coef['d']
    eta = a + b*Temp + c*np.power(Temp, 2) + d*np.power(Temp, 3)
    eta /= 1000.0  # m Pa s -> Pa s (cP -> Pa s)
    return eta


def Janzetal1968exp(Temp, coef=coef):
    r"""Return dynamic viscosity according to Janz et  al. (1968) p. 1.

    .. math:: \eta = a \exp(b/R T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., Dampier, F.W., Lakshminarayanan, G.R., Lorenz, P.K.,
    Tomkins, R.P.T., 1968. Molten salts (No. 15), Nat. Stand. Ref. Data Ser.
    National Bureau of Standards, Washington.

    """
    R = 1.98717  # cal mol^-1 deg^-1
    A = coef['A']
    E = coef['E']
    eta = A*np.exp(E/(R*Temp))
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def KostalMalek2010(Temp, coef=coef):
    r"""Return dynamic viscosity according to Kostal, Malek (2010).

    p. 2804, Fig. 1

    .. math:: ln(\eta) = A + B / (T - T0)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Koštál, P., Málek, J., 2010. Viscosity of selenium melt. Journal of
    Non-Crystalline Solids 356, 2803–2806.

    """
    try:
        A = coef['A']
        B = coef['B']
        T0 = coef['T0']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = A + B/(Temp - T0)
    eta = np.exp(eta)
    return eta  # already in Pa s


def Ohse1985(Temp, coef=coef):
    r"""Return dynamic viscosity according to Ohse (1985) p. 771.

    .. math:: ln(\eta) = a + b ln(T) + c/T

    \eta in Pa s
    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Ohse, R.W. (Ed.), 1985. Handbook of thermodynamic and transport
    properties of alkali metals, International union of pure and applied
    chemistry chemical data series. Blackwell Scientific Publications,
    Oxford, London, Edinburgh, Boston, Palo Alto, Melbourne.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = a + b*np.log(Temp) + c/Temp
    eta = np.exp(eta)
    return eta  # already in Pa s


def Linstrom1992plusE(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Janz (1992) eq. E+.

    .. math:: \eta = D1 \exp(D2/(R T))

    T in K

    added the possibility to modify R  in order to use the equation
    for the data of Tasidou et al. (2019) - and possibly others -
    as well

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    if 'R' not in coef.keys():
        R = 8.31441
    else:
        R = coef['R']
    try:
        D1 = coef['D1']
        D2 = coef['D2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = D1*np.exp(D2/(R*Temp))
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992DP(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. DP.

    .. math:: \eta = D1

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = D1 / 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992E1(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. E1.

    .. math:: \eta = D1 \exp(D2/(8.31441 T)) + D3/T^2

    T in K

    QUESTIONABLE EQUATION, functional comment in Linstrom1992 -> sort out
    """
    R = 8.31441
    D1 = coef['D1']
    D2 = coef['D2']
    D3 = coef['D3']
    eta = D1*np.exp(D2/(R*Temp)) + D3/np.power(Temp, 2)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992E2(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. E2.

    .. math:: \eta = D1 \exp(D2/(8.31441 (T - D3)))

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    R = 8.31441
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = D1*np.exp(D2/(R*(Temp-D3)))
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992P2(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. P2.

    .. math:: \eta = D1 + D2 * T + D3 * T^2

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = D1 + D2*Temp + D3*np.power(Temp, 2)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992P3(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. P3.

    .. math:: \eta = D1 + D2 * T + D3 * T^2 + D4 * T^3

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298

    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = D1 + D2*Temp + D3*np.power(Temp, 2) + D4*np.power(Temp, 3)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992I1(Temp, *args, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. I1.

    .. math:: \eta = D1 + D2 C

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    eta = D1 + D2*C
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992I2(Temp, *args, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. I2.

    .. math:: \eta = D1 + D2 C + D3 C^2

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    eta = D1 + D2*C + D3*np.power(C, 2)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992I3(Temp, *args, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. I3.

    .. math:: \eta = D1 + D2 C + D3 C^2 + D4 C^3

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    eta = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def Linstrom1992I4(Temp, *args, coef=coef):
    r"""Return the dynamic viscosity according to Linstrom (1992) eq. I4.

    .. math:: \eta = D1 + D2 C + D3 C^2 + D4 C^3 + D5 C^4

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.

    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        D5 = coef['D5']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    eta = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3) + D5*np.power(C, 4)
    eta /= 1000.0  # m Pa s -> Pa s
    return eta


def ToerklepOeye1982(Temp, coef=coef):
    r"""Return the dynamic viscosity according to Toerklep, Oeye (1982) eqs. (6-9).

    .. math:: \eta = A exp(B/T + C/T^4)

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Dynamic viscosity in Pa s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Toerklep, K., Oeye, H.A., 1982. Viscostiy of molten alkaline-earth
    chlorides. Journal of Chemical & Engineering Data 27, 387–391.
    https://doi.org/10.1021/je00030a006

    """
    try:
        A = coef['A']
        B = coef['B']
        C = coef['C']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    eta = A*np.exp(B/Temp + C/np.power(Temp, 2))
    eta /= 1000.0  # m Pa s -> Pa s
    return eta
