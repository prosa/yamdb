"""Implements the volume change on fusion method."""
coef = None


def SchinkeSauerwald1956(coef=coef):
    r"""Return volume change on fusion: Delta Vf as percentage value
    relative to the solid volume Vs

    .. math:: \Delta V_f / V_s

    as used, e.g., by Bockris et al (1962) and Schinke, Sauerwald (1956).

    Parameters
    ----------
    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        volume change on fusion in %.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Bockris, J.O’M., Pilla, A., Barton, J.L., 1962. The compressibilities of
    certain molten alkaline earth halides and the volume change upon fusion
    of some of the corresponding solids. Revue de Chimie Acad. Rep.
    Populaire Roumaine 7, 59–77.

    Schinke, H., Sauerwald, F., 1956. Dichtemessungen. XVIII. Über die
    Volumenänderung beim Schmelzen und den Schmelzprozeß bei Salzen.
    Zeitschrift für anorganische und allgemeine Chemie 287, 313–324.
    """
    try:
        dV = coef['dV']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return dV

