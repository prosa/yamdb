"""Implements the density methods."""
import numpy as np

coef = None


def Assaeletal2012(Temp, coef=coef):
    r"""Return density according to Assael et al. (2012) p. 6, eq. (1), tab. 2.

    .. math:: \rho = c1 - c2(T - T_{ref})

    Similar to Steinberg1974, but T_ref may differ
    (should be T_m, but not sure if this is always the same value).

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Assael, M.J., Armyra, I.J., Brillo, J., Stankus, S.V., Wu, J., Wakeham,
    W.A., 2012. Reference data for the density and viscosity of liquid
    cadmium, cobalt, gallium, indium, mercury, silicon, thallium, and zinc.
    Journal of Physical and Chemical Reference Data 41, 033101.

    Steinberg, D.J., 1974. A simple relationship between the temperature
    dependence of the density of liquid metals and their boiling
    temperatures. Metallurgical Transactions 5, 1341–1343.
    """
    try:
        c1 = coef['c1']
        c2 = coef['c2']
        Tref = coef['Tref']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = c1 - c2*(Temp - Tref)
    return rho


def Bockrisetal1962(Temp, coef=coef):
    r"""Return density according to Bockris et al. (1962) p. 66, tab. 6.
    Tref defaults to 273.15 K because Bockris et al. (1962) used
    degrees C in their equation
    
    .. math:: \rho = a - b(T - T_\text{ref}) - c(T - T_\text{ref})^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Bockris, J.O’M., Pilla, A., Barton, J.L., 1962. The compressibilities of
    certain molten alkaline earth halides and the volume change upon fusion
    of some of the corresponding solids. Revue de Chimie Acad. Rep.
    Populaire Roumaine 7, 59–77.
    """
    Tref = 273.15
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    if 'Tref' in coef.keys():
        Tref = coef['Tref']
    rho = a - b*(Temp - Tref) - c*(Temp - Tref)*(Temp - Tref)
    return rho


def DoboszGancarz2018(Temp, coef=coef):
    r"""Return density according to Dobosz, Gancarz (2018).

    .. math:: \rho = a_1 * T + a_2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Dobosz, A., Gancarz, T., 2018. Reference data for the density,
    viscosity, and surface tension of liquid Al-Zn, Ag-Sn, Bi-Sn, Cu-Sn, and
    Sn-Zn eutectic alloys. Journal of Physical and Chemical Reference Data
    47, 013102.
    """
    try:
        a_1 = coef['a1']
        a_2 = coef['a2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return a_1 * Temp + a_2


def IAEA2008(Temp, coef=coef):
    r"""Return density according to IAEA (2008) eq. (3.29), p. 87.

    .. math:: \rho = \rho_0 * (a + b*t + c*t^2 + d*t^3 + e*t^4)

    t means obviously temperature in deg C.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.
    """
    try:
        rho_0 = coef['rho_0']
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
        e = coef['e']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    t = Temp - 273.15
    rho = rho_0 * (a + b*t + c*np.power(t, 2) +
                   d*np.power(t, 3) + e*np.power(t, 3))
    return rho


def Janzetal1975TC(Temp, *args, coef=coef):
    r"""Return density according to Janz et al. (1975) p. 970, Tab. 231.

    .. math:: \rho = a + b T + c C + d C^2

    C is the fraction of the first component in mol%.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., Tomkins, R.P.T., Allen, C.B., Downey, J.R., Garner, G.L.,
    Krebs, U., Singer, S.K., 1975. Molten salts: Volume 4, part 2, chlorides
    and mixtures - electrical conductance, density, viscosity, and surface
    tension data. Journal of Physical and Chemical Reference Data 4,
    871–1178.

    Examples
    --------
    To get the density of CaCl2-NaCl 30-70 at 650 °C (if available)

    >>> CaCl2_NaCl.composition['range'].density(650 + 273.15, 30)
    """
    try:
        if isinstance(args[0], np.ndarray):
            # x = args[0].copy()
            # TODO: Implement case discrimination for different array-float combinations
            raise ValueError('Mole fraction arrays not yet implemented, please use'
                             ' scalars!')
        else:
            x = args[0]
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise

    rho = a + b*Temp + c*x + d*np.power(x, 2)

    rho *= 1000.0  # g/cm3 -> kg/m3

    return rho


def Janzetal1975TC2(Temp, *args, coef=coef):
    r"""Return density according to Janz et al. (1975) p. 1162, Tab. 622.

    .. math:: \rho = a + b T + c C + d T C^2 + e C T^2

    C is the fraction of the first component in mol%.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., Tomkins, R.P.T., Allen, C.B., Downey, J.R., Garner, G.L.,
    Krebs, U., Singer, S.K., 1975. Molten salts: Volume 4, part 2, chlorides
    and mixtures-electrical conductance, density, viscosity, and surface
    tension data. Journal of Physical and Chemical Reference Data 4,
    871–1178. https://doi.org/10.1063/1.555527
    """
    try:
        if isinstance(args[0], np.ndarray):
            # x = args[0].copy()
            # TODO: Implement case discrimination for different array-float combinations
            raise ValueError('Mole fraction arrays not yet implemented, please use'
                             ' scalars!')
        else:
            x = args[0]
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
        e = coef['e']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise

    rho = a + b*Temp + c*x + d*np.power(x, 2)*Temp + e*x*np.power(Temp, 2)

    rho *= 1000.0  # g/cm3 -> kg/m3

    return rho


def Shpilrain1985(Temp, coef=coef):
    r"""Return density according to Shpilrain et al. (1985).

    See p. 453/454, eq. (15), tab. 2.

    .. math:: \rho = \sum_{i=0..m}(a[i]*\tau^i)

    .. math:: \tau = Temp/1000

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Shpil’rain, E.E., Yakimovich, K.A., Fomin, V.A., Skovorodjko, S.N.,
    Mozgovoi, A.G., 1985. Density and thermal expansion of liquid alkali
    metals, in: Ohse, R.W. (Ed.), Handbook of Thermodynamic and Transport
    Properties of Alkali Metals, International Union of Pure and Applied
    Chemistry Chemical Data Series. Blackwell Scientific Publications, pp.
    435–469.
    """
    try:
        a = coef['a']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = 0
    tau = Temp/1000.0
    for i, c in enumerate(a):
        rho += c * np.power(tau, i)
    rho *= 1000.0  # apparently needed to convert to kg/m^3
    return rho


def Sobolev2011(Temp, coef=coef):
    r"""Return density according to Sobolev (2011).

    .. math:: \rho = \rho_s + drhodT * T

    This is in contrast to Eq. (3.1) of Sobolev2011,
    but probably just what is used.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.
    """
    try:
        rho_s = coef['rho_s']
        drhodT = coef['drhodT']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return rho_s + drhodT * Temp


def Steinberg1974(Temp, coef=coef):
    r"""Return density according to Iida, Guthrie (1988), eq. (3.19), p. 70.

    .. math:: \rho = \rho_m - \lambda  (T - T_m)

    Note that the sign of lambda is inverted with respect to the
    original definition.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 1988. The physical properties of liquid
    metals. Clarendon Press, Oxford.

    Steinberg, D.J., 1974. A simple relationship between the temperature
    dependence of the density of liquid metals and their boiling
    temperatures. Metallurgical Transactions 5, 1341–1343.
    """
    try:
        rho_m = coef['rho_m']
        lambda_ = coef['lambda']
        Tm = coef['Tm']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return rho_m - lambda_ * (Temp - Tm)


def Linstrom1992DP(Temp, coef=coef):
    r"""Return the density according to Janz (1992) eq. DP.

    .. math:: \rho = D1

    i.e. constant.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        D1 = coef['D1']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = D1 * 1000.0  # g/cm^3 -> kg/m^3
    return rho


def Linstrom1992P1(Temp, coef=coef):
    r"""Return the density according to Linstrom (1992) eq. P1.

    .. math:: \rho = D1 + D2 * T

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = D1 + D2*Temp
    rho *= 1000.0  # g/cm^3 -> kg/m^3
    return rho


def Linstrom1992P2(Temp, coef=coef):
    r"""Return the density according to Linstrom (1992) eq. P2.

    .. math:: \rho = D1 + D2 * T + D3 * T^2

    T in K

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = D1 + D2*Temp + D3*np.power(Temp, 2)
    rho *= 1000.0  # g/cm^3 -> kg/m^3
    return rho


def Linstrom1992I1(Temp, *args, coef=coef):
    r"""Return density according to Linstrom (1992) eq. I1.

    .. math:: \rho = D1 + D2 C

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits)**.

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    rho = D1 + D2*C
    rho *= 1000.0  # g/cm3 -> kg/m3
    return rho


def Linstrom1992I2(Temp, *args, coef=coef):
    r"""Return density according to Linstrom (1992) eq. I2.

    .. math:: \rho = D1 + D2 C + D3 C^2

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    rho = D1 + D2*C + D3*np.power(C, 2)
    rho *= 1000.0  # g/cm3 -> kg/m3
    return rho


def Linstrom1992I3(Temp, *args, coef=coef):
    r"""Return density according to Linstrom (1992) eq. I3.

    .. math:: \rho = D1 + D2 C + D3 C^2 + D4 C^3

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    rho = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3)
    rho *= 1000.0  # g/cm3 -> kg/m3
    return rho


def Linstrom1992I4(Temp, *args, coef=coef):
    r"""Return density according to Linstrom (1992) eq. I4.

    .. math:: \rho = D1 + D2 C + D3 C^2 + D4 C^3 + D5 C^4

    C is the fraction of the first component in mol%.
    **T = Tmin (obtain by using get_equation_limits).**

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array, **ignored**.

    *args : float
        Fraction of the first component in mol%.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Density in kg/m3, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Janz, G.J., 1992. Data from: NIST properties of molten salts database
    (formerly SRD 27). https://doi.org/10.18434/MDS2-2298.
    """
    try:
        if isinstance(args[0], np.ndarray):
            x = args[0].copy()
        else:
            x = args[0]
        D1 = coef['D1']
        D2 = coef['D2']
        D3 = coef['D3']
        D4 = coef['D4']
        D5 = coef['D5']
        component = coef['component']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    C = x
    if component == "second":
        C = 100.0 - C
    rho = D1 + D2*C + D3*np.power(C, 2) + D4*np.power(C, 3) + D5*np.power(C, 4)
    rho *= 1000.0  # g/cm3 -> kg/m3
    return rho
