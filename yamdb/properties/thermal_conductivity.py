"""Implements the thermal_conductivity methods."""
import numpy as np

coef = None


def Assaeletal2017(Temp, coef):
    r"""Return thermal conductivity according to Assael et al. (2017).

    p. 16, eq. (2)

    .. math:: \lambda = c1 + c2*(T - Tm)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Assael, M.J., Chatzimichailidis, A., Antoniadis, K.D., Wakeham, W.A.,
    Huber, M.L., Fukuyama, H., 2017. Reference correlations for the thermal
    conductivity of liquid copper, gallium, indium, iron, lead, nickel and
    tin. High Temperatures - High Pressures 46, 391–416.

    """
    try:
        c1 = coef['c1']
        c2 = coef['c2']
        Tm = coef['Tm']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = c1 + c2*(Temp - Tm)
    return lambda_


def Chliatzouetal2018(Temp, coef):
    r"""Return thermal conductivity according to Chliatzou et al. (2018).

    p. 7, eq. (1), p. 8, Tab. 5

    .. math:: \lambda = c0 + c1*(T - Tm)

    thermal conductivity is computed in mW/(m K) in the
    original publication

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Chliatzou, Ch.D., Assael, M.J., Antoniadis, K.D., Huber, M.L., Wakeham,
    W.A., 2018. Reference correlations for the thermal conductivity of 13
    inorganic molten salts. Journal of Physical and Chemical Reference Data
    47, 033104.

    """
    try:
        c0 = coef['c0']
        c1 = coef['c1']
        Tm = coef['Tm']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = c0 + c1*(Temp - Tm)
    lambda_ /= 1000.0  # mW/(m K) -> W/(m K)
    return lambda_


def IAEA2008(Temp, coef):
    r"""Return thermal conductivity according to IAEA (2008).

    p. 87, eq. (3.31)

    .. math:: \lambda = a + b*T + c*T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    t = Temp - 273.15
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = a + b*t + c*np.power(t, 2)
    return lambda_


def Sobolev2011(Temp, coef):
    r"""Return thermal conductivity according to OECDNEA (2015).

    p. 126, eq. (2.91)

    .. math:: \lambda = a + b*T + c*T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = a + b*Temp + c*np.power(Temp, 2)
    return lambda_


def SquareRoot(Temp, coef):
    r"""Return fit function value of lambda.

    originally for Cs data of Iida, Guthrie (2015) p. 536, Tab. 17.11

    .. math:: \lambda = a + b*T + c*\sqrt{T}

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 2015. The thermophysical properties of
    metallic liquids. Oxford University Press, Oxford.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = a + b*Temp + c*np.power(Temp, 0.5)
    return lambda_


def Touloukian1970b(Temp, coef):
    r"""Return thermal conductivity according to OECDNEA (2015).

    p. 125, eq. (2.90)

    .. math:: \lambda = a + b*T

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Thermal conductivity in W/(m K), single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    OECD Nuclear Energy Agency, Nuclear Science Committee, 2015. Handbook on
    lead-bismuth eutectic alloy and lead properties, materials
    compatibility, thermal-hydraulics and technologies. OECD/NEA/NSC.

    """
    try:
        a = coef['a']
        b = coef['b']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    lambda_ = a + b*Temp
    return lambda_
