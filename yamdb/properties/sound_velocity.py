"""Implements the sound_velocity methods."""
coef = None


def Blairs2007(Temp, coef=coef):
    """Return sound velocity according to Blairs (2007).

    U = A + B * T

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Sound velocity m/s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Blairs, S., 2007. Review of data for velocity of sound in pure liquid
    metals and metalloids. International Materials Reviews 52, 321–344.

    """
    try:
        A = coef['A']
        B = coef['B']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return A + B * Temp


def Blairs2007cubic(Temp, coef=coef):
    r"""Return sound velocity according to Blairs (2007).

    cubic equation p. 337

    .. math:: U = A + B * T + C * T^2

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Sound velocity m/s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Blairs, S., 2007. Review of data for velocity of sound in pure liquid
    metals and metalloids. International Materials Reviews 52, 321–344.

    """
    try:
        A = coef['A']
        B = coef['B']
        C = coef['C']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    return A + B * Temp + C * Temp * Temp


def IAEA2008(Temp, coef=coef):
    """Return sound velocity according to IAEA (2008), p. 87, eq. (3.37).

    U = a + b/T + c*(Tm - d)

    quite strange to combine Kelvin and Celsius temperatures
    in one equation range

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Sound velocity m/s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    v = a + b/Temp + c*(Temp - 273.15)
    return v


def linearExperimental(Temp, coef=coef):
    """Return sound velocity according to Iida, Guthrie (1988).

    U = Um - m_dUdT * T

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Sound velocity m/s, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Iida, T., Guthrie, R.I.L., 1988. The physical properties of liquid
    metals. Clarendon Press, Oxford.

    """
    try:
        Um = coef['Um']
        m_dUdT = coef['m_dUdT']
        Tm = coef['Tm']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    U = Um - (Temp - Tm) * m_dUdT
    return U
