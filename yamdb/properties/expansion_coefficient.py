"""Implements the expansion_coefficient methods."""
import numpy as np

coef = None


def IAEA2008(Temp, coef=coef):
    r"""Return volumetric expansion coefficient according to IAEA (2008).

    p. 87, eq. (3.34)

    .. math:: \beta * 10^4 = a + b*t + c*t^2 + d*t^3

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Volumetric expansion coefficient in 1/K, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    International Atomic Energy Agency, 2008. Thermophysical properties of
    materials for nuclear engineering: A tutorial and collection of data.
    International Atomic Energy Agency, Vienna.

    """
    try:
        a = coef['a']
        b = coef['b']
        c = coef['c']
        d = coef['d']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    t = Temp - 273.15
    beta = a + b*t + c*np.power(t, 2) + d*np.power(t, 3)
    beta /= 1.0E04
    return beta


def OECDNEA2015(Temp, coef=coef):
    r"""Return volumetric expansion coefficient according to OECD/NEA (2015).

    p. 78, eq. (2.34)

    .. math:: \beta = 1/(a-T)

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Volumetric expansion coefficient in 1/K, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    OECD Nuclear Energy Agency, Nuclear Science Committee, 2015. Handbook on
    lead-bismuth eutectic alloy and lead properties, materials
    compatibility, thermal-hydraulics and technologies. OECD/NEA/NSC.

    """
    try:
        a = coef['a']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    beta = 1.0/(a - Temp)
    return beta


def Steinberg1974(Temp, coef=coef):
    r"""Return expansion coefficient according to Iida, Guthrie (2015).

    p. 511 footnote :math:`\dagger\dagger`

    .. math:: \beta = -\lambda / \rho

    tabulated :math:`\lambda` is :math:`- \lambda` !

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Volumetric expansion coefficient in 1/K, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Steinberg, D.J., 1974. A simple relationship between the temperature
    dependence of the density of liquid metals and their boiling
    temperatures. Metallurgical Transactions 5, 1341–1343.

    Iida, T., Guthrie, R.I.L., 2015. The thermophysical properties of
    metallic liquids. Oxford University Press, Oxford.

    """
    try:
        rho_m = coef['rho_m']
        lambda_ = coef['lambda']
        Tm = coef['Tm']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = rho_m - lambda_ * (Temp - Tm)
    beta = lambda_ / rho
    return beta


def Sobolev2011(Temp, coef=coef):
    r"""Return expansion coefficient according to Sobolev (2011).

    p. 67 effectively Eqs. (3-10) ff.

    .. math:: \beta = - 1/\rho \partial \rho / \partial T

    Parameters
    ----------
    Temp : float
        Temperature in K, single value or NumPy array.

    coef : float
        List of coefficients, typically automatically extracted from YAML DB.

    Returns
    -------
    float
        Volumetric expansion coefficient in 1/K, single value or NumPy array.

    Raises
    ------
    LookupError
        If not all required keys/values are present in the coef dictionary.

    References
    ----------
    Sobolev, V., 2011. Database of thermophysical properties of liquid metal
    coolants for GEN-IV (No. SCK•CEN-BLG-1069). SCK•CEN.

    """
    try:
        rho_s = coef['rho_s']
        drhodT = coef['drhodT']
    except LookupError as e:
        print("ERROR: Not all coefficients supplied, cannot compute values!\n"
              "First missing coefficient: %s" % e)
        raise
    rho = rho_s + drhodT * Temp
    return - drhodT/rho
