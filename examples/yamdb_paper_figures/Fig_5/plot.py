"""Reproduce the CaCl2-NaCl surface tension plot of Fig. 5 

   Weier, T., Nash, W., Personnettaz, P., Weber, N., 2024. Yamdb: Easily
   accessible thermophysical properties of liquid metals and molten salts.
   Journal of Open Research Software.
"""
import numpy as np
import matplotlib.pyplot as plt
import yamdb.yamdb as ydb


def read_gnuplot_data(fname):
    """read data digitized by digit.tcl"""
    with open(fname, 'r') as fp:
        data = fp.read()
    ddict = {}
    for line in data.splitlines():
        if line.startswith("#"):
            key = line.lstrip("#").strip().replace(" ", "_")
            ddict[key] = []
        elif len(line) > 0 and line[0].isdigit() or line.startswith('-'):
            ddict[key].append([float(x) for x in line.split()])
    for key in ddict:
        ddict[key] = np.array(ddict[key])
    return ddict


# Addison, C.C., Coldrey, J.M., 1960. Influence of surface reactions on
# the interface between liquid sodium and molten sodium chloride + calcium
# chloride mixtures. Transactions of the Faraday Society 56, 840.
# https://doi.org/10.1039/tf9605600840
AC_file = "Addison_Coldrey_1960_Fig3.dat"
AC_lines_file = "Addison_Coldrey_1960_Fig3_lines.dat"
AC_solid_file = "Addison_Coldrey_1960_Fig3_solid-liquid.dat"

src1975 = 'Janzetal1975'
src1988 = 'Janz1988'

# data from database salts.yml
CaCl2_NaCl = ydb.get_from_salts('CaCl2-NaCl')
keys1975 = CaCl2_NaCl.get_compositions_with_property_source('surface_tension',
                                                            src1975)
keys1988 = CaCl2_NaCl.get_compositions_with_property_source('surface_tension',
                                                            src1988)

AC_dat = read_gnuplot_data(AC_file)
AC_line_dat = read_gnuplot_data(AC_lines_file)
AC_solid_dat = read_gnuplot_data(AC_solid_file)

fig, ax = plt.subplots()

# loop over temperatures
for key in AC_dat.keys():
    if key != 'liquidus':
        ax.plot(AC_dat[key][:, 0], AC_dat[key][:, 1]/1000.0, 'o')
        ax.plot(AC_line_dat[key][:, 0], AC_line_dat[key][:, 1]/1000.0, '-',
                color=plt.gca().lines[-1].get_color())
        temp = float(key.replace('_°C', '')) + 273.15
        concentrations = []
        surface_tensions = []
        # loop over compositions with source Janzetal1975
        for c_key in keys1975:
            concentrations.append(float(c_key.split('-')[0]))
            surface_tensions.append(CaCl2_NaCl.composition[c_key].surface_tension(temp,
                                                                                  source=src1975))
        ax.plot(concentrations,
                surface_tensions,
                color=plt.gca().lines[-1].get_color(),
                linestyle='dotted')
        
        concentrations = []
        surface_tensions = []
        # loop over compositions with source Janz1988
        for c_key in keys1988:
            concentrations.append(float(c_key.split('-')[0]))
            surface_tensions.append(CaCl2_NaCl.composition[c_key].surface_tension(temp,
                                                                                  source=src1988))
        ax.plot(concentrations,
                surface_tensions,
                color=plt.gca().lines[-1].get_color(),
                linestyle='dashed',
                label=('%d °C' % (temp - 273.15)))

x = np.linspace(0, 100, num=100)
y = np.interp(x,
              AC_solid_dat['solid_liquid'][:, 0],
              AC_solid_dat['solid_liquid'][:, 1]/1000.0)

ax.fill_between(x, y, y2=0.16, color='w', zorder=2, ec='k')
ax.annotate('solid', xy=(16, 0.137))
ax.annotate('solid', xy=(74, 0.146))
ax.annotate('liquid', xy=(16, 0.109))

ax.legend(handlelength=0.7, frameon=False)

ax.set_xlabel(r"x$_{\mathdefault{CaCl_{\mathdefault{2}}}}$ / mol%")
ax.set_ylabel(r"$\gamma$ / Nm$^{\mathdefault{-1}}$")
ax.set_xlim([0, 100])
ax.set_ylim([0.097, 0.15])

fig.subplots_adjust(
    left=0.11,
    bottom=0.10,
    right=0.976,
    top=0.98,
    hspace=0,
    wspace=0,
    )

fig.savefig("plot.pdf")
