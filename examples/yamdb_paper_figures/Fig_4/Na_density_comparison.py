"""Reproduce the Na density vs. temperature plot of Fig. 4 

   Weier, T., Nash, W., Personnettaz, P., Weber, N., 2024. Yamdb: Easily
   accessible thermophysical properties of liquid metals and molten salts.
   Journal of Open Research Software.
"""
import numpy as np
import matplotlib.pyplot as plt

import yamdb.yamdb as ydb

Na = ydb.get_from_metals('Na')

# data below acquired in August 2023 from
#
# https://www.nist.gov/mml/acmd/trc/nist-alloy-data
#
# by copying and editing the "Metals Alloy Data Table" available under
# "Open the Data Table" of the respective source (e.g. column "Author":
# "Davies", column "Year Published": "1972", column "Properties":
# "specific density") 
#
# Data provided via the NIST Alloy data web application, DOI: 10.18434/M32153
#
# Pfeif, E.A., Kroenlein, K., 2016. Perspective: Data infrastructure for
# high throughput materials discovery. APL Materials 4, 053203.
# https://doi.org/10.1063/1.4942634
#
# Wilthan, B., Pfeif, E.A., Diky, V.V., Chirico, R.D., Kattner, U.R.,
# Kroenlein, K., 2017. Data resources for thermophysical properties of
# metals and alloys, Part 1: Structured data capture from the archival
# literature. Calphad 56, 126–138.
# https://doi.org/10.1016/j.calphad.2016.12.004

# Khairulin, R.A., Stankus, S.V., Abdullaev, R.N., 2013. Density, thermal
# expansion and binary diffusion coefficients of sodium-lead melts. High
# Temperatures - High Pressures 42, 493–507.
fname = 'Khairulin_et_al_2013_Density_thermal_expansion_and_binary_diffusion_coefficients_of_sodium-lead_melts.dat'

# Taova, T.M., Mal’surgenova, F.M., Alchagirov, B.B., Khokonov, Kh.L.,
# 2009. The density and molar volumes of ternary alloys of cross sections
# of the sodium-potassium-cesium system technically important
# temperatures. High Temperature 47, 815–821.
fname2 = 'Taova_et_al_2009_The_density_and_molar_volumes.dat'

# Ruppersberg, H., Jost, J., 1989. Determination of the heat capacity of
# liquid alloys according to the (∂p/∂t)s procedure: Pb/Na. Thermochimica
# Acta 151, 187–195.
fname3 = 'Ruppersberg_Jost_1989_Determination_of_the_heat_capacity.dat'

# Davies, H.A., 1972. The density and surface tension of dilute liquid
# Na-In alloys and comparison with liquid Na-Cd alloys. Metallurgical
# Transactions 3, 2917–2921.
fname4 = 'Davies_1972_The_density_and_surface_tension.dat'

conv = {
    0: lambda x: float(x),
    1: lambda x: float(x),
    2: lambda x: float(x[1:]),
    }

conv2 = {
    0: lambda x: float(x),
    1: lambda x: float(x[1:]),
    2: lambda x: float(x),
    3: lambda x: float(x[1:]),
    }

data = np.loadtxt(fname, skiprows=1, converters=conv)
data2 = np.loadtxt(fname2, skiprows=1, converters=conv)
data3 = np.loadtxt(fname3, skiprows=1, converters=conv)
data4 = np.loadtxt(fname4, skiprows=1, converters=conv2)

t = np.linspace(data[:, 0].min(), data[:, 1].max())

fig, ax = plt.subplots()

for source in Na.get_source_list('density'):
    ax.plot(t, Na.density(t, source=source), label=source)

ax.errorbar(data4[:, 0], data4[:, 2], xerr=data4[:, 1], yerr=data4[:, 3], fmt='o', linewidth=2, capsize=6, label='Davies (1972)')
ax.errorbar(data3[:, 0], data3[:, 1], yerr=data3[:, 2], fmt='o', linewidth=2, capsize=6, label='Ruppersberg, Jost (1989)')
ax.errorbar(data2[:, 0], data2[:, 1], yerr=data2[:, 2], fmt='o', linewidth=2, capsize=6, label='Taova et al. (2009)')
ax.errorbar(data[:, 0], data[:, 1], yerr=data[:, 2], fmt='o', linewidth=2, capsize=6, label='Khairulin et al. (2013)')

ax.legend(frameon=False)

ax.set_xlabel('T / K')
ax.set_ylabel(r'ρ / kg m$^\mathdefault{-3}$')
fig.subplots_adjust(
    left=0.10,
    bottom=0.09,
    right=0.99,
    top=0.99,
    hspace=0,
    wspace=0,
    )

fig.savefig('Na_density_comparison.pdf')
