Fig. 4 of Weier et al. (2024)

The auxiliary data in the files

    - Davies_1972_The_density_and_surface_tension.dat 
    - Khairulin_et_al_2013_Density_thermal_expansion_and_binary_diffusion_coefficients_of_sodium-lead_melts.dat
    - Ruppersberg_Jost_1989_Determination_of_the_heat_capacity.dat
    - Taova_et_al_2009_The_density_and_molar_volumes.dat

are based on Davies (1972), Khairulin et al. (2013), Ruppersberg and
Jost (1989), and Taova et al. (2009). They were copied from the NIST
Alloy Data website:

https://www.nist.gov/mml/acmd/trc/nist-alloy-data

Data provided via the NIST Alloy data web application, DOI:
10.18434/M32153

Please see as well Pfeif and Kroenlein (2016) and Wilthan et al. (2017).

Thanks to the National Institute of Standards and Technology (NIST) for
making these data publicly available!

Reproduce the figure with

    python Na_density_comparison.py

References

Davies, H.A., 1972. The density and surface tension of dilute liquid
Na-In alloys and comparison with liquid Na-Cd alloys. Metallurgical
Transactions 3, 2917–2921.

Khairulin, R.A., Stankus, S.V., Abdullaev, R.N., 2013. Density, thermal
expansion and binary diffusion coefficients of sodium-lead melts. High
Temperatures - High Pressures 42, 493–507.

Pfeif, E.A., Kroenlein, K., 2016. Perspective: Data infrastructure for
high throughput materials discovery. APL Materials 4, 053203.
https://doi.org/10.1063/1.4942634

Ruppersberg, H., Jost, J., 1989. Determination of the heat capacity of
liquid alloys according to the (∂p/∂t)s procedure: Pb/Na. Thermochimica
Acta 151, 187–195.

Taova, T.M., Mal’surgenova, F.M., Alchagirov, B.B., Khokonov, Kh.L.,
2009. The density and molar volumes of ternary alloys of cross sections
of the sodium-potassium-cesium system technically important
temperatures. High Temperature 47, 815–821.

Weier, T., Nash, W., Personnettaz, P., Weber, N., 2024. Yamdb: Easily
accessible thermophysical properties of liquid metals and molten salts.
Journal of Open Research Software.

Wilthan, B., Pfeif, E.A., Diky, V.V., Chirico, R.D., Kattner, U.R.,
Kroenlein, K., 2017. Data resources for thermophysical properties of
metals and alloys, Part 1: Structured data capture from the archival
literature. Calphad 56, 126–138.
https://doi.org/10.1016/j.calphad.2016.12.004
