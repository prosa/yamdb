import numpy as np
import matplotlib.pyplot as plt
import yamdb.yamdb as ydb

Na = ydb.get_from_metals('Na')

dsources = Na.get_source_list('density')
vsources = Na.get_source_list('dynamic_viscosity')

T = np.linspace(Na.Tm, Na.Tb)

fig, ax = plt.subplots()

for dsource in dsources:
    for vsource in vsources:
        ax.plot(T,
                (Na.dynamic_viscosity(T, source=vsource) /
                 Na.density(T, source=dsource)),
                label="%s-%s" % (vsource, dsource))
ax.legend(frameon=False)
ax.set_xlabel("T / K")
ax.set_ylabel("ν / m s⁻¹")
ax.set_ylim([0.2E-06, 1E-06])

fig.savefig("kinematic_viscosity_Na.png")
