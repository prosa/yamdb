#!/usr/bin/env python
import os
from importlib import resources as ir
import numpy as np
import matplotlib.pyplot as plt
from yamdb import yamdb as ydb

mf = os.path.join(ir.files('yamdb'), 'data', 'metals.yml')
sdb = ydb.SubstanceDB(mf)

# metal_strings = [
#     "Bi",
#     "Ga",
#     "Hg",
#     "Li",
#     "Na",
#     "Pb",
#     "Zn",
# ]

# to check all metals in database:
metal_strings = list(sdb.materials.keys())

properties = [
    "density",
    "dynamic_viscosity",
    "expansion_coefficient",
    "heat_capacity",
    "resistivity",
    "sound_velocity",
    "surface_tension",
    "thermal_conductivity",
    "vapour_pressure",
    "volume_change_fusion",
]

ylabel = {
    "density": r"$\rho$ / kg m$^{-3}$",
    "dynamic_viscosity": r"$\eta$ / Pa s",
    "expansion_coefficient": r"$\beta$ / K$^{-1}$",
    "heat_capacity": r"c$_p$ / J kg$^{-1}$ K$^{-1}$",
    "resistivity": r"$\rho_e$ / $\Omega$ m",
    "sound_velocity": r"U / m s$^{-1}$",
    "surface_tension": r"$\sigma$ / N m$^{-1}$",
    "thermal_conductivity": r"$\lambda$ / W m$^{-1}$ K$^{-1}$",
    "vapour_pressure": r"p$_v$ / Pa",
    "volume_change_fusion": r"$\Delta$V/V$_\mathdefault{solid}$ / %",
}

p = {}

for metal in metal_strings:
    p[metal] = ydb.Properties(sdb.get_substance(metal))
    for property_ in properties:
        t = np.linspace(p[metal].Tm, p[metal].Tb, num=1000)
        if property_ in p[metal].get_property_list():
            fig, ax = plt.subplots()
            func = getattr(p[metal], "%s" % property_)
            for source in p[metal].get_source_list(property_):
                if property_ == "volume_change_fusion":
                    ax.plot([p[metal].Tm], [func(source=source)], '*', label=source)
                else:
                    g = ax.plot(t, func(t, source=source), label=source)
                    # get color
                    gc = g[0].get_color()
                    # check for temperature limits
                    Tmin, Tmax = p[metal].get_equation_limits(property_, source)
                    if Tmin is not None and Tmin >= t.min():
                        ax.plot(Tmin, func(Tmin, source=source), marker="$[$", color=gc)
                    if Tmax is not None and Tmax <= t.max():
                        ax.plot(Tmax, func(Tmax, source=source), marker="$]$", color=gc)
            ax.legend()
            ax.set_title(metal + " %s" % property_.replace("_", " "))
            ax.set_xlabel("T / K")
            ax.set_ylabel(ylabel[property_])
            fig.tight_layout()
            fig.savefig("%s_%s.png" % (metal, property_))
            plt.close()
        else:
            print("%s: %s missing" % (metal, property_))
